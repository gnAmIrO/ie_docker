package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student;

import ie.ut.ac.ir.Bolbolestan.services.model.API.APIHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.Exception.ErrorHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.Helper.Constants;
import ie.ut.ac.ir.Bolbolestan.services.model.JSON.JsonExtractor;
import ie.ut.ac.ir.Bolbolestan.services.model.JSON.JsonHandler;
import org.json.JSONArray;
import org.json.JSONObject;


import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StudentRepoInterface {
    private static final JsonExtractor jsonExtractor;
    static {
        jsonExtractor = new JsonExtractor();
    }
    public static String AddNewStudent(JSONObject obj) throws SQLException {
        Set<String> addNewStudentKeys = Stream.of(Constants.STUDENT_ID, Constants.NAME, Constants.LAST_NAME, Constants.BIRTH_DATE, Constants.FIELD, Constants.FACULTY, Constants.LEVEL, Constants.STATUS, Constants.IMG, Constants.EMAIL, Constants.PASSWORD).collect(Collectors.toSet());
        Map<String, Object> values = jsonExtractor.Extract(obj, addNewStudentKeys);

        String studentId = (String) values.get(Constants.STUDENT_ID);
//        System.out.println((String) values.get(Constants.EMAIL));
//        System.out.println((String) values.get(Constants.PASSWORD));

        try {
            StudentRepository.GetInstance().AddStudent((String) values.get(Constants.NAME),
                    (String) values.get(Constants.LAST_NAME),
                    studentId,
                    (String) values.get(Constants.ENTERED_AT),
                    (String) values.get(Constants.BIRTH_DATE),
                    (String) values.get(Constants.FIELD),
                    (String) values.get(Constants.FACULTY),
                    (String) values.get(Constants.LEVEL),
                    (String) values.get(Constants.STATUS),
                    (String) values.get(Constants.IMG),
                    (String) values.get(Constants.EMAIL),
                    (String) values.get(Constants.PASSWORD)
                    );

        } catch (ErrorHandler errorHandler) {
            System.out.println("Student Exists");
        }

        APIHandler apiHandler = new APIHandler();
        String response = apiHandler.RequestToAPI(String.format(Constants.GradesUrl + "/%s", studentId));
        JSONArray grades = JsonHandler.DeserializeJsonArray(response);
        StudentInterface.GetInstance().UpdateGrades(studentId, grades);


        return Constants.OK;
    }
    public static String AddNewStudents(JSONArray students) throws ErrorHandler, SQLException {
        for (int i = 0; i < students.length(); ++i) {
            AddNewStudent(students.getJSONObject(i));
        }
        return Constants.OK;
    }


    public static Student GetStudent(JSONObject obj) throws ErrorHandler {
        Set<String> getStudent = Stream.of(Constants.STUDENT_ID).collect(Collectors.toSet());
        Map<String, Object> values = jsonExtractor.Extract(obj, getStudent);
        String studentId = (String) values.get(Constants.STUDENT_ID);

        return GetStudent(studentId);
    }

    public static Student GetStudent(String studentId)  {
        return StudentRepository.GetInstance().GetStudent(studentId);
    }

    public static boolean DoesStudentExist(String id) {
        return StudentRepository.GetInstance().DoesStudentExist(id);
    }

}
