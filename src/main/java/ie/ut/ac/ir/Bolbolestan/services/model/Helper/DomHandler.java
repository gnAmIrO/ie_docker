package ie.ut.ac.ir.Bolbolestan.services.model.Helper;

public class DomHandler {

    public static String HtmlHeaderRender(String title, String style){
        String htmlContent = "<head>\n"                       +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>"                    +
                title                            +
                "    </title>\n"                 +
                "    <style>\n"                  +
                style                            +
                "    </style>\n"                 +
                "</head>\n"                      ;
        return htmlContent;
    }

    public static String WrapInTag(String tag, String id, String style, String content){
        return "<" + tag + " id=\"" + id + "\" style=\" " + style + "\">" + content + "</"+ tag + ">\n";
    }
}
