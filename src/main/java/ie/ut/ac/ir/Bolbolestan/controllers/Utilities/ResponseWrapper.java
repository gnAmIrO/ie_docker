package ie.ut.ac.ir.Bolbolestan.controllers.Utilities;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;

public class ResponseWrapper {
    private String content;
    private HttpStatus status;

    public ResponseWrapper(){}
    public ResponseWrapper(String content, HttpStatus status){
        this.content = content;
        this.status = status;
    }

    public void setContent(String content) { this.content = content;}
    public void setStatus(HttpStatus status){ this.status = status;}

    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("content", content);
        jsonObject.put("status", status);

        return jsonObject.toString();
    }
}
