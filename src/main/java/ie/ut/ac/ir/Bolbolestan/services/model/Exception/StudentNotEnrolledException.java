package ie.ut.ac.ir.Bolbolestan.services.model.Exception;

public class StudentNotEnrolledException extends Throwable {
    public StudentNotEnrolledException(String errorMessage) {
        super(errorMessage);
    }
}
