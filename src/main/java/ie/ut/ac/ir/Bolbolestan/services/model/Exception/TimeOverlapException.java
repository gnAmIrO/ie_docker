package ie.ut.ac.ir.Bolbolestan.services.model.Exception;

public class TimeOverlapException extends Throwable{
    public TimeOverlapException(String message) {
        super(message);
    }
}
