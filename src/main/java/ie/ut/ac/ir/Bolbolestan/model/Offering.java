package ie.ut.ac.ir.Bolbolestan.model;

public class Offering {
    private String studentId;
    private String courseId;
    private int fullStatus;
    private int deleteStatus;
    private int submitStatus;
//    private boolean submitStatus;
//    private boolean deleteStatus;

    public Offering(String studentId, String courseId, int full, int deleteStatus, int submitStatus){
        this.studentId      = studentId;
        this.courseId       = courseId;
        this.fullStatus     = full;
        this.deleteStatus   = deleteStatus;
        this.submitStatus   = submitStatus;
    }

    public String GetStudentId(){ return studentId; }
    public String GetCourseId() { return courseId; }

    public int GetFullStatus() { return fullStatus; }
    public int GetDeleteStatus() { return deleteStatus; }
    public int GetSubmitStatus() { return submitStatus; }
}