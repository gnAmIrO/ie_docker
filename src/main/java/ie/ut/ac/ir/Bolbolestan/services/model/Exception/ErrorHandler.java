package ie.ut.ac.ir.Bolbolestan.services.model.Exception;

public class ErrorHandler extends Throwable {
    private String message;

    public ErrorHandler(String message) { this.message = message; }

    public String Message() { return message; }
}


