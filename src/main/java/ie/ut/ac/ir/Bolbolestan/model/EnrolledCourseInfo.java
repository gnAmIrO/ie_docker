package ie.ut.ac.ir.Bolbolestan.model;

import org.json.JSONObject;

public class EnrolledCourseInfo {
    private String studentId;
    private String courseId;
    private int term;
    private float score;
    private String courseName;
    private int unit;

    public EnrolledCourseInfo(String studentId, String courseId, int term, int unit, float score, String courseName){
        this.studentId = studentId;
        this.courseId = courseId;
        this.term = term;
        this.score = score;
        this.unit = unit;
        this.courseName = courseName;
    }

    public int GetTerm() {
        return term;
    }

    public String GetStudentNumberId() {
        return studentId;
    }


    public float GetGrade() {
        return score;
    }

    public String GetCourseId() {
        return courseId;
    }

    public int GetUnit() {
        return unit;
    }

    public String GetCourseName() {
        return courseName;
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        obj.put("id", courseId);
        obj.put("grade", score);
        obj.put("numOfUnits", unit);
        obj.put("term", term);
        obj.put("name", courseName);
        return obj;
    }
}
