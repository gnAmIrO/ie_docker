package ie.ut.ac.ir.Bolbolestan.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class Repository<T, I> {

    abstract protected String getFindByIdStatement(Object obj);

    abstract protected void fillFindByIdValues(PreparedStatement st, Object obj) throws SQLException;


    abstract protected String getInsertStatement();

    abstract protected void fillInsertValues(PreparedStatement st, T data) throws SQLException;

    abstract protected String getFindAllStatement(Object obj);

    abstract protected void fillFindAllValues(PreparedStatement st, Object obj) throws SQLException;

    abstract protected T convertResultSetToDomainModel(ResultSet rs) throws SQLException;

    abstract protected ArrayList<T> convertResultSetToDomainModelList(ResultSet rs) throws SQLException;



    public T findById(Object obj) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(getFindByIdStatement(obj));
        fillFindByIdValues(st, obj);
        try {
            ResultSet resultSet = st.executeQuery();
            if (!resultSet.next()) {
                st.close();
                con.close();
                return null;
            }
            T result = convertResultSetToDomainModel(resultSet);
            st.close();
            con.close();
            return result;
        } catch (Exception e) {
            st.close();
            con.close();
            System.out.println("error in Repository.find query.");
            // e.printStackTrace();
            throw e;
        }
    }



    public void insert(T obj) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(getInsertStatement());
        try {
            fillInsertValues(st, obj);
            st.execute();
            st.close();
            con.close();
        } catch (Exception e ) {
            st.close();
            con.close();
            System.out.println("error in Repository.insert query.");
//            e.printStackTrace();
            throw e;
        }
    }

    public List<T> findAll(Object obj) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(getFindAllStatement(obj));
//        System.out.println(st.toString());
        try {
            fillFindAllValues(st, obj);
            ResultSet resultSet = st.executeQuery();
            if (resultSet == null) {
                st.close();
                con.close();
                return new ArrayList<>();
            }
            List<T> result = convertResultSetToDomainModelList(resultSet);
            st.close();
            con.close();
            return result;
        } catch (Exception e) {
            st.close();
            con.close();
            System.out.println("error in Repository.findAll query.");
            e.printStackTrace();
            throw e;
        }
    }

    public List<T> findAll(String stmt) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(stmt);
        try {
            ResultSet resultSet = st.executeQuery();
            if (resultSet == null) {
                st.close();
                con.close();
                return new ArrayList<>();
            }
            List<T> result = convertResultSetToDomainModelList(resultSet);
            st.close();
            con.close();
            return result;
        } catch (Exception e) {
            st.close();
            con.close();
            System.out.println("error in Repository.findAll query.");
            // e.printStackTrace();
            throw e;
        }
    }





    public void transact(Object obj) throws SQLException {
//        System.out.println(stmt);
        Connection con = ConnectionPool.getConnection();
        PreparedStatement preparedStatement = con.prepareStatement(getTransactionStatement(obj));
        try {
            fillTransactionValues(preparedStatement, obj);
            preparedStatement.execute();
            preparedStatement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("SQLState: " +
                    ((SQLException)e).getSQLState());

            System.err.println("Error Code: " +
                ((SQLException)e).getErrorCode());

            System.err.println("Message: " + e.getMessage());
            System.out.println("couldn't transact to table");
            preparedStatement.close();
            con.close();
        }
    }

    protected abstract void fillTransactionValues(PreparedStatement preparedStatement, Object obj) throws SQLException;

    protected abstract String getTransactionStatement(Object obj);
}
