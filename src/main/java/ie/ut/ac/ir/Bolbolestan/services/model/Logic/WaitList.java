package ie.ut.ac.ir.Bolbolestan.services.model.Logic;

import ie.ut.ac.ir.Bolbolestan.model.Offering;
import ie.ut.ac.ir.Bolbolestan.repository.OfferingRepo;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.Student;

import java.sql.SQLException;
import java.util.Set;

public class WaitList {
//    private Set<String> finalizedWaitList;
//    private Set<String> nonFinalizedWaitList;
    private OfferingRepo repository = OfferingRepo.getInstance();
    private Student student;
    public WaitList(Student student) {
        this.student = student;
//        this.finalizedWaitList    = new HashSet<>();
//        this.nonFinalizedWaitList = new HashSet<>();
    }

    public Set<String> GetList() {
        try {
            return repository.FindSameOffersByStudentId(student.GetInfo().GetId(), 1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public void Reset() {
//        nonFinalizedWaitList.clear();
//        nonFinalizedWaitList.addAll(finalizedWaitList);
        try {
            repository.ResetOffers(student.GetInfo().GetId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("ResetWait --> Error in submitting courses");
        }
    }

    public void Add(String courseId) {
        Offering newOffer = new Offering(student.GetInfo().GetId(), courseId, 1, 0, 0);
        try {
            repository.insert(newOffer);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("Add --> Error in inserting new Offer");
        }
    }

    public void Remove(String courseId) {
        System.out.println("HELLO from wait");
        try {
            repository.DeleteSelectedOffer(student.GetInfo().GetId(), courseId, 1);
        } catch (SQLException throwables) {
            System.out.println("Remove --> Unable to delete course, Error in query");
        }
    }

    public boolean DoesExist(String courseId) {
        String courseClassCode = courseId.substring(0, courseId.length() - 2);
        try {
            if (repository.FindOfferBy(student.GetInfo().GetId(), courseClassCode + "01", 1) != null) return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("DoesExist --> Error in finding course in db");
        }
        return false;
    }

    public void Finalize() {
        try {
            repository.SubmitOffers(student.GetInfo().GetId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("Finalize --> Error in submitting courses");
        }
    }

    public Set<String> GetRemovedCourses() {
        Set<String> removedCourses = null;
        try {
            removedCourses = repository.findRemovedOffers(student.GetInfo().GetId(), 1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return removedCourses;
    }

    public Set<String> GetFinalizedCourses() {
        try {
            return repository.FindOffersByStudentId(student.GetInfo().GetId(), 1, 1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("GetFinalizedCourses --> Unable to Get finalized courses");
            return null;
        }
    }
}
