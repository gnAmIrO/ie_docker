package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course;
import ie.ut.ac.ir.Bolbolestan.model.Prerequisite;
import ie.ut.ac.ir.Bolbolestan.repository.CourseRepo;
import ie.ut.ac.ir.Bolbolestan.repository.OfferingRepo;
import ie.ut.ac.ir.Bolbolestan.repository.PrerequisiteRepo;
import org.javatuples.Pair;

import java.sql.SQLException;
import java.util.*;

public class CourseRepository {
    private static Map<String, Course> courses;
    private static CourseRepository instance;
    private CourseRepo courseRepo = CourseRepo.getInstance();
    private PrerequisiteRepo preRepo = PrerequisiteRepo.getInstance();

    static {
        courses =  new TreeMap<String, Course>();
    }

    public static CourseRepository GetInstance(){
        if (instance == null) {
            instance = new CourseRepository();
        }
        return instance;
    }


    public void AddCourse(Course course) {
        try {
            courseRepo.insert(course);
        } catch (SQLException throwable) {
//            throwable.printStackTrace();
            System.out.println("35 --> Unable to insert new course to db");
        }
        String id = course.GetInfo().GetId();
        courses.put(id, course);

    }

    public Course GetCourse(String courseId) {

        try {
            Course course = courseRepo.findById(courseId);
//            System.out.println("44 --> " + course.GetInfo().GetClassId());
            course.GetInfo().SetPrerequisites(preRepo.findPrerequisites(course.GetInfo().GetId()));
            return course;
        } catch (SQLException throwables) {
            // throwables.printStackTrace();
            System.out.println("50 --> Unable to Get course from db");
            return null;
        }

    }

    public Map<Pair<String, String>, String> GetOverlappingCourses(Set<String> courses, String selectedCourse) {
        Map<Pair<String, String>, String> overlappingCourses = new HashMap<>();
        for (String courseIdA: courses) {
            if (courseIdA.equals(selectedCourse)) continue;

            System.out.println(courseIdA);
            System.out.println(selectedCourse);
            int overlapStatus = Course.DoCoursesOverlap(GetCourse(courseIdA), GetCourse(selectedCourse));
            if (overlapStatus != 0) {
                if ((overlapStatus & 1) == 1) {
                    overlappingCourses.put(new Pair<>(courseIdA, selectedCourse), "ExamTime");
                }
                if ((overlapStatus & 2) == 2) {
                    overlappingCourses.put(new Pair<>(courseIdA, selectedCourse), "ClassTime");
                }
            }

        }
        return overlappingCourses;
    }

    public List<Course> GetFullCourses(Set<String> coursesToCheck) { // TODO: better name?
//        List<String> fullCourses = new ArrayList<>();
//
//        for (String courseId: coursesToCheck) {
//            Course course = GetCourse(courseId);
//            if (course.IsCourseFull()) {
//                fullCourses.add(courseId);
//            }
//        }
        try {
            List<Course> result = new ArrayList<>();
            courseRepo.getFullCourses().forEach(course -> {
                if (coursesToCheck.contains(course)) {
                    try {
                        course.GetInfo().SetPrerequisites(preRepo.findPrerequisites(course.GetInfo().GetId()));
                    } catch (SQLException throwables) {
                        System.out.println("87 --> Error in reading Data from prerequisite table");
                    }
                    result.add(course);
                }
            });
            return result;

        } catch (SQLException throwables) {
            return null;
        }
    }

    public void EnrollStudent(String studentId, Set<String> courses) {
        for (String courseId: courses) {
            Course course = GetCourse(courseId);
            course.EnrollStudent(studentId);
        }
    }

    public void UnEnrollStudent(String studentId, Set<String> courses) {
        for (String courseId : courses) {
            Course course = GetCourse(courseId);
            course.UnEnrollStudent(studentId);
        }
    }

    public void AddStudentToWaitLists(String studentId, Set<String> courses) {
        for (String courseId: courses) {
            Course course = GetCourse(courseId);
            course.AddWaiter(studentId);
        }
    }

    public void EnrollWaiters() {
//        List<Course> courses = GetAllCourses("", "All");
        OfferingRepo offerRepository = OfferingRepo.getInstance();
        Set<String> courses = null;
        try {
            courses = offerRepository.FindWaitedCourses();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        for (String courseId: courses) {
            Course course = GetCourse(courseId);
            course.EnrollWaiters();
        }
    }


    public List<Course> GetAllCourses(String searchFilter, String typeFilter) {
        try {
            List<Course> courses = courseRepo.getOfferings(searchFilter, typeFilter);
            for(Course course: courses){
                course.GetInfo().SetPrerequisites(preRepo.findPrerequisites(course.GetInfo().GetId()));
            }
            return courses;
        } catch (SQLException throwables) {
            return null;
        }
    }


    public void AddCoursePrerequisite(Prerequisite pr) {
        try {
            preRepo.insert(pr);
        } catch (SQLException throwables) {
            System.out.println("prerequisite doesn't exist in courses??!");
        }
    }
}
