package ie.ut.ac.ir.Bolbolestan.repository.helper;

public class OfferCommandInfo {
    private final OfferInfo info;
    private final OfferCommandType commandType;

    public OfferCommandInfo(OfferInfo info, OfferCommandType commandType) {
        this.info = info;
        this.commandType = commandType;
    }

    public OfferInfo GetInfo() {
        return info;
    }

    public OfferCommandType GetCommandType() {
        return commandType;
    }
}
