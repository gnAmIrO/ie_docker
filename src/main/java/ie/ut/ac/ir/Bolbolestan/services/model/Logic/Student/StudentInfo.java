package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student;

import ie.ut.ac.ir.Bolbolestan.model.EnrolledCourseInfo;
import ie.ut.ac.ir.Bolbolestan.repository.EnrolledCoursesRepo;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StudentInfo {
    private EnrolledCoursesRepo repository = EnrolledCoursesRepo.getInstance();
    private String name;
    private String lastName;
    private String fullName;
    private String id;
    private String email;
    private String password;
    private String field;
    private String faculty;
    private String level;
    private String status;
    private String img;
    private String entryYear;
    private String  birthDate;
    private Map<String, EnrolledCourseInfo> enrolledCourses;

    public StudentInfo(String name, String lastName, String id, String entryYear, String birthDateText, String field, String faculty, String level, String status, String img, String email, String password) {

        this.name      = name;
        this.lastName  = lastName;
        this.fullName  = name + " " + lastName;
        this.id        = id;
        this.entryYear = entryYear;
        this.birthDate = birthDateText;
        this.field     = field;
        this.faculty   = faculty;
        this.level     = level;
        this.status    = status;
        this.img       = img;
        this.email     = email;
        this.password  = password;

        enrolledCourses = new HashMap<>();
    }

    public void HashPassword(){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        this.password  = bCryptPasswordEncoder.encode(password);
    }

    public String GetId() {
        return id;
    }
    public String GetFirstName() { return name; }
    public String GetLastName() { return lastName; }
    public String GetBirthDate() { return birthDate; }
    public String GetField() { return field; }
    public String GetFaculty() { return faculty; }
    public String GetLevel() { return level; }
    public String GetStatus() { return status; }
    public String GetImg() { return img; }
    public String GetEmail() { return email; }
    public String GetPassword() { return password; }


    private String FindCorrespondingEnrolledCourse(String courseId) {

        for (String enrolledId : enrolledCourses.keySet()) {
            String enrolledCourseId = enrolledId.substring(0, enrolledId.length() - 2);
            if (courseId.equals(enrolledCourseId))
                return enrolledId;
        }
        return "";
    }

    public boolean IsCourseEnrolledByStudent(String courseId) {

        if (enrolledCourses.containsKey(courseId)) return true;
        return !FindCorrespondingEnrolledCourse(courseId).equals("");
    }

    public float GetEnrolledCourseGrade(String courseId)  {
        if (!IsCourseEnrolledByStudent(courseId)) return 0.0f;
        String enrolledId = FindCorrespondingEnrolledCourse(courseId);
        return enrolledCourses.get(enrolledId).GetGrade();
    }
    public boolean IsCoursePassed(String courseId)  {
        return IsCourseEnrolledByStudent(courseId) && (GetEnrolledCourseGrade(courseId) > 10.0f);
    }
    public boolean HasPassedAllPrerequisites(Set<String> prerequisites) {
        UpdateGrades();
        for (String prerequisiteId: prerequisites) {
            if (!prerequisiteId.equals("") && !IsCoursePassed(prerequisiteId)) {
                return false;
            }
        }
        return true;
    }



    public double CalcGPU(List<EnrolledCourseInfo> ce){
        double s = 0.0;
        int numOfPassedUnits = 0;
        for(EnrolledCourseInfo course : ce){
            System.out.println(course.GetCourseId());
            int courseNumOfUnits = course.GetUnit();
            numOfPassedUnits += courseNumOfUnits;
            s += course.GetGrade() * courseNumOfUnits;
        }
        if(numOfPassedUnits == 0)
            return 0;

        return s / (double)numOfPassedUnits;
    }

    public int GetNumOfPassedUnits(List<EnrolledCourseInfo> ce){
        int numOfPassedUnits = 0;
        for(EnrolledCourseInfo course : ce){
            numOfPassedUnits += course.GetUnit();
        }

        return numOfPassedUnits;
    }
    
    public Map GetEnrolledCourses(){
        return enrolledCourses;
    }



    @Override
    public boolean equals(Object obj) {

        if (obj == this)
            return true;

        if (!(obj instanceof StudentInfo))
            return false;

        StudentInfo studentInfo = (StudentInfo) obj;

        return id.equals(studentInfo.GetId());
    }

//    public void UpdateGrade(EnrolledCourse enrolledCourse) {
//        String courseId = enrolledCourse.GetId();
//        enrolledCourses.put(courseId, enrolledCourse);
//    }

    public void UpdateGrades(){
        List<EnrolledCourseInfo> ce;
        try {
            ce = repository.GetEnrolledCoursesInfo(id);
        } catch (SQLException throwables) {
            System.out.println("Error in Getting EnrolCourse from repository");
            ce = null;
        }
        for(EnrolledCourseInfo course : ce){
            enrolledCourses.put(course.GetCourseId(), course);
        }
    }


    public JSONObject toJson() { //TODO add email to JSON
        List<EnrolledCourseInfo> ce;
        try {
            ce = repository.GetEnrolledCoursesInfo(id);
        } catch (SQLException throwables) {
            System.out.println("Error in Getting EnrolCourse from repository");
            ce = null;
        }
        JSONObject obj = new JSONObject();

        JSONObject info = new JSONObject();

        info.put("name", fullName);
        info.put("id", GetId());
        info.put("birthDate", GetBirthDate());
        info.put("GPU", CalcGPU(ce));
        info.put("numPassedUnits", GetNumOfPassedUnits(ce));
        info.put("faculty", GetFaculty());
        info.put("field", GetField());
        info.put("level", GetLevel());
        info.put("status", GetStatus());
        info.put("img", GetImg());

        obj.put("info", info);

        JSONObject termEnrolledCourses = new JSONObject();
        Map<String, Float> gradesPerTerm = new HashMap<>();
        Map<String, Integer> unitsPerTerm = new HashMap<>();
        for (EnrolledCourseInfo course: ce) {
            String term = Integer.toString(course.GetTerm());
            if (!termEnrolledCourses.has(term)) {
                JSONObject semester = new JSONObject();
                semester.put("courses", new JSONArray());
                termEnrolledCourses.put(term, semester);
                gradesPerTerm.put(term, 0.0f);
                unitsPerTerm.put(term, 0);
            }
            ((JSONArray)((JSONObject)termEnrolledCourses.get(term)).get("courses")).put(course.toJson());
            gradesPerTerm.replace(term, gradesPerTerm.get(term) + course.GetGrade() * course.GetUnit());
            unitsPerTerm.replace(term, unitsPerTerm.get(term) + course.GetUnit());
        }

        for (String term: termEnrolledCourses.keySet()) {
            ((JSONObject)termEnrolledCourses.get(term)).put("gpu", gradesPerTerm.get(term) / unitsPerTerm.get(term));
        }



        obj.put("semesters", termEnrolledCourses);
        return obj;
    }

}
