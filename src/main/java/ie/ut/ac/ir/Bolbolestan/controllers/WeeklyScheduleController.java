package ie.ut.ac.ir.Bolbolestan.controllers;

import ie.ut.ac.ir.Bolbolestan.controllers.Utilities.ResponseWrapper;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.Course;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.CourseRepoInterface;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.Student;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.StudentRepoInterface;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@RestController
public class WeeklyScheduleController {


    private JSONArray CreateResponseData(String stdId) {
        Student student = StudentRepoInterface.GetStudent(stdId);
        Set<String> finalizedCoursesIds = student.GetFinalizedCourses();

        List<Course> courses = CourseRepoInterface.GetCourses(new ArrayList<>(finalizedCoursesIds));
        JSONArray obj = new JSONArray();
        courses.forEach(course -> {obj.put(course.GetInfo().toJson());});
        return obj;
    }

    @RequestMapping(value = "/weekly_schedule", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> GetData(){
        ResponseWrapper responseWrapper;
        String stdId =  SecurityContextHolder.getContext().getAuthentication().getName();

        return new ResponseEntity<>(CreateResponseData(stdId).toString(), HttpStatus.OK);

    }


}
