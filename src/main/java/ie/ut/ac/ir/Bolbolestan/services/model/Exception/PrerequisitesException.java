package ie.ut.ac.ir.Bolbolestan.services.model.Exception;

public class PrerequisitesException extends Throwable{
    public PrerequisitesException(String message) {
        super(message);
    }
}
