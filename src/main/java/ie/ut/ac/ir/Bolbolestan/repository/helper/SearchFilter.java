package ie.ut.ac.ir.Bolbolestan.repository.helper;

public class SearchFilter {
    private final String typeFilter;
    private final String searchInput;

    public SearchFilter(String typeFilter, String searchInput) {
        this.typeFilter = typeFilter;
        this.searchInput = searchInput;
    }

    public String GetSearchInput() {
        return searchInput;
    }

    public String GetTypeFilter() {
        return typeFilter;
    }
}
