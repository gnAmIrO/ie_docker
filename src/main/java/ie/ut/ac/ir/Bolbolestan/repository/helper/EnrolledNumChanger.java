package ie.ut.ac.ir.Bolbolestan.repository.helper;

public class EnrolledNumChanger {
    private final boolean isIncrease;
    private final String courseId;

    public EnrolledNumChanger(boolean isIncrease, String courseId) {
        this.isIncrease = isIncrease;
        this.courseId = courseId;
    }

    public boolean IsIncrease() {
        return isIncrease;
    }

    public String GetCourseId() {
        return courseId;
    }
}
