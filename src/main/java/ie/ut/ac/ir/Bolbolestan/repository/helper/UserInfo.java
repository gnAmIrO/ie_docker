package ie.ut.ac.ir.Bolbolestan.repository.helper;

public class UserInfo {
    private final String id;
    private final String password;
    private final boolean isIdEmail;

    public UserInfo(String id, String password, boolean isIdEmail) {
        this.id = id;
        this.password = password;
        this.isIdEmail = isIdEmail;
    }

    public String GetId() {
        return id;
    }

    public String GetPassword() {
        return password;
    }

    public boolean isIdEmail() {
        return isIdEmail;
    }
}
