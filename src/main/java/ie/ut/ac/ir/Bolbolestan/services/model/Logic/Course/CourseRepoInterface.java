package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course;

import ie.ut.ac.ir.Bolbolestan.model.Prerequisite;
import ie.ut.ac.ir.Bolbolestan.repository.CourseRepo;
import ie.ut.ac.ir.Bolbolestan.services.model.Exception.ErrorHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.Student;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.StudentRepoInterface;
import org.javatuples.Pair;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time.ClassTime;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time.ExamTime;
import org.json.JSONObject;
import org.json.JSONArray;

import java.sql.SQLException;
import java.util.*;

public class CourseRepoInterface {
    public static String AddNewCourses(JSONArray newCourses) throws ErrorHandler, SQLException {
        for (int i = 0; i < newCourses.length(); i++) {
            AddNewCourse(newCourses.getJSONObject(i));
        }
        for (int i = 0; i < newCourses.length(); i++){
            AddCoursesPrerequisites(newCourses.getJSONObject(i));
        }
        return "OK";
    }

    private static void AddCoursesPrerequisites(JSONObject newCourseJson) {
        String classCode = (String)newCourseJson.get("classCode");
        if (classCode.length() == 1) {
            classCode = "0" + classCode;
        }

        String courseId = (String)newCourseJson.get("code") + classCode;
        Set<String> prerequisites = GetNewCoursePrerequisites((JSONArray) newCourseJson.get("prerequisites"));
        for (String coursePrerequisite : prerequisites) {
            Prerequisite pr = new Prerequisite(courseId, coursePrerequisite);
            CourseRepository.GetInstance().AddCoursePrerequisite(pr);
        }
    }

    public static String AddNewCourse(JSONObject newCourseJson) throws ErrorHandler, SQLException {
        String CourseId = (String)newCourseJson.get("code");

        JSONObject examTimeJsonData = (JSONObject)newCourseJson.get("examTime");
        ExamTime newExamTime = new ExamTime((String)examTimeJsonData.get("start"), (String)examTimeJsonData.get("end"));

        JSONObject classTimeJsonData = (JSONObject)newCourseJson.get("classTime");
        ClassTime newClassTime = new ClassTime(GetClassDays((JSONArray) classTimeJsonData.get("days")),(String) classTimeJsonData.get("time"));
        String classCode = (String)newCourseJson.get("classCode");
        if (classCode.length() == 1) {
            classCode = "0" + classCode;
        }
        Course newCourse = new Course((String)newCourseJson.get("name"), (String)newCourseJson.get("code"), classCode, (String)newCourseJson.get("instructor"), (Integer)newCourseJson.get("units"), newClassTime, newExamTime, (Integer)newCourseJson.get("capacity"), (String)newCourseJson.get("type"), 0);

        CourseRepository.GetInstance().AddCourse(newCourse);
        return "OK";
    }

    private static Set<String> GetNewCoursePrerequisites(JSONArray prerequisites){
        Set<String> prerequisitesSet = new HashSet<>();
        for(int i = 0; i < prerequisites.length(); i++){
            prerequisitesSet.add(prerequisites.getString(i));
        }
        return prerequisitesSet;
    }

    private static List<String> GetClassDays(JSONArray classTimes){
        List<String> classDays = new ArrayList<>();
        for(int i = 0; i < classTimes.length(); i++){
            classDays.add(classTimes.getString(i));
        }
        return classDays;
    }

    private static boolean DoesCourseExist(String newCourseCode){
        Course SimilarCourses = GetCourse(newCourseCode);

        return SimilarCourses != null;
    }

    public static List<Course> GetCourses(List<String> courseIds) {
        List<Course> courses = new ArrayList<>();
        for (String courseId: courseIds) {
            System.out.println("GetCourses --> " + courseId);
            courses.add(GetCourse(courseId));
        }
        return courses;
    }

    public static Course GetCourse(String CourseCode){
        return CourseRepository.GetInstance().GetCourse(CourseCode);
    }


    public static List<Course> GetAllCourses(String searchFilter, String typeFilter){
        return CourseRepository.GetInstance().GetAllCourses(searchFilter, typeFilter);
    }

    public static List<Course> GetFullCourses(Set<String> courses) {
        return CourseRepository.GetInstance().GetFullCourses(courses);
    }

    public static void EnrollStudent(String studentId, Set<String> courses) {
        CourseRepository.GetInstance().EnrollStudent(studentId, courses);
    }

    public static void AddStudentToWaitLists(String studentId, Set<String> courses) {
        CourseRepository.GetInstance().AddStudentToWaitLists(studentId, courses);
    }

    public static Map<Pair<String, String>, String> GetOverlappingCourses(Set<String> courses, String selectedCourseId) { return CourseRepository.GetInstance().GetOverlappingCourses(courses, selectedCourseId); }

    public static void UnEnrollStudent(String studentId, String courseId) {
        Course course = GetCourse(courseId);
        course.UnEnrollStudent(studentId);
    }

    public static void UnEnrollStudent(String studentId, Set<String> courses) {
        CourseRepository.GetInstance().UnEnrollStudent(studentId, courses);
    }

    public static void RemoveStudentFromWaitList(String studentId, String courseId) {
        Course course = GetCourse(courseId);
        course.RemoveWaiter(studentId);
    }


    public static void RemoveStudentFromCoursesWaitList(String studentId, Set<String> courses) {
        for (String courseId : courses) {
            RemoveStudentFromWaitList(studentId, courseId);
        }
    }
}
