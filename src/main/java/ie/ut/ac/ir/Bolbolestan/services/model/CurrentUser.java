package ie.ut.ac.ir.Bolbolestan.services.model;

public class CurrentUser {
    private static CurrentUser instance = null;

    private String userId = "";

    public static CurrentUser GetInstance(){
        if( instance == null ) instance = new CurrentUser();

        return instance;
    }

    public void SetUserId(String value) { userId = value; }
    public String GetUserId(){ return userId; }

    public void Delete() {
        instance = null;
        userId = "";

    }
}
