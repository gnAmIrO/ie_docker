package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ie.ut.ac.ir.Bolbolestan.services.model.JSON.JsonHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.Course;
import org.json.JSONObject;



public class ExamTime {
    private Interval time;
    private String begin;
    private String end;

    public ExamTime(String begin, String end) {
        this.begin = begin;
        this.end = end;
        try {
            this.time = ExtractTimeFromString(begin, end);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    // TODO: feature envy?
    private Interval ExtractTimeFromString(String begin, String end) throws ParseException {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        begin = begin.replace("T", " ");
        end   = end.replace  ("T", " ");

        Date dateBegin = df.parse(begin);
        Date dateEnd   = df.parse(end);

        long epochBegin = dateBegin.getTime();
        long epochEnd = dateEnd.getTime();

        return new Interval(epochBegin, epochEnd);
    }

    public static boolean DoesTimeOverlap(Course courseA, Course courseB) {
        return DoesTimeOverlap(courseA.GetInfo().GetExamTime(), courseB.GetInfo().GetExamTime());
    }

    public static boolean DoesTimeOverlap(ExamTime examTimeA, ExamTime examTimeB) {
        return Interval.HasOverlap(examTimeA.GetTime(), examTimeB.GetTime());
    }

    @Override
    public String toString() {
        JSONObject examTimeJson = new JSONObject();

        examTimeJson.put("start", begin);
        examTimeJson.put("end", end);

        return JsonHandler.SerializeJsonData(examTimeJson);
    }

    public static ExamTime ConvertToClass(JSONObject examTimeObject){
        return new ExamTime(examTimeObject.getString("start"), examTimeObject.getString("end"));
    }

    public Interval GetTime() {
        return time;
    }
    public String GetBeginTime(){ return begin; }
    public String GetEndTime(){ return end; }
}
