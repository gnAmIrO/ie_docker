package ie.ut.ac.ir.Bolbolestan.services.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SubmitFailedMessage {
    private static SubmitFailedMessage instance = null;

    private List<String> FailedMessages = new ArrayList<>();

    public static SubmitFailedMessage GetInstance(){
        if( instance == null ) instance = new SubmitFailedMessage();

        return instance;
    }

    public void AddMessage(String value) { FailedMessages.add(value);}
    public void AddMessage(List<String> values) {
        FailedMessages = Stream.concat(FailedMessages.stream(), values.stream())
            .collect(Collectors.toList());
    }
    public  List<String> GetMessages(){ return FailedMessages; }

    public void Delete() {
        instance = null;
        FailedMessages = new ArrayList<>();
    }

    @Override
    public String toString(){
        return FailedMessages.stream().map(Object::toString).collect(Collectors.joining("\n"));
    }
}

