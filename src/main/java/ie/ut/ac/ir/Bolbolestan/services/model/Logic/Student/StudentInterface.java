package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student;

import ie.ut.ac.ir.Bolbolestan.model.EnrolledCourseInfo;
import ie.ut.ac.ir.Bolbolestan.repository.EnrolledCoursesRepo;
import ie.ut.ac.ir.Bolbolestan.services.model.Helper.Constants;
import ie.ut.ac.ir.Bolbolestan.services.model.JSON.JsonExtractor;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.Course;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.CourseRepoInterface;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.EnrolledCourse;
import ie.ut.ac.ir.Bolbolestan.services.model.Exception.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class StudentInterface {
    private static final JsonExtractor jsonExtractor;
    static {
        jsonExtractor = new JsonExtractor();
    }
    private EnrolledCoursesRepo repository = EnrolledCoursesRepo.getInstance();
    private static StudentInterface instance;
    public static StudentInterface GetInstance(){
        if (instance == null) {
            instance = new StudentInterface();
        }
        return instance;
    }

    public void EditWeeklySchedule(String command, String studentId, String courseId) throws ErrorHandler, CourseAlreadyAddedException, StudentNotEnrolledException, PrerequisitesException, TimeOverlapException {
        Student student = StudentRepoInterface.GetStudent(studentId);

        if(CourseRepoInterface.GetCourse(courseId) == null) throw new ErrorHandler(Constants.OFFERING_NOT_FOUND);

        if (command.equals(Constants.ADD_TO_WEEKLY_SCHEDULE)) {
            student.AddCourseToWeeklySchedule(courseId);
        }
        else if (command.equals(Constants.REMOVE_FROM_WEEKLY_SCHEDULE)) {
            student.RemoveCourseFromWeeklySchedule(courseId);
        }
    }

    public List<Course> GetWeeklySchedule(String studentId) {
        Student student = StudentRepoInterface.GetStudent(studentId);
        if (student == null)
            return null;
        List<String> enrolledCoursesIds = student.GetWeeklySchedule();
        List<Course> weeklySchedule = new ArrayList<>();

        for(String CourseId: enrolledCoursesIds){
            Course enrolledCourse = CourseRepoInterface.GetCourse(CourseId);
            weeklySchedule.add(enrolledCourse);
        }
        return weeklySchedule;
    }

    public String HandleFinalizeWeeklySchedule(String studentId) throws FinalizeException, ErrorHandler{
        Student student  = StudentRepoInterface.GetStudent(studentId);
        student.HandleFinalizeWeeklySchedule();
        return Constants.OK;
    }

//    public void DeleteNonFinalizedCourses(String studentId) throws ErrorHandler {
//        Student student = StudentRepoInterface.GetStudent(studentId);
//        student.DeleteNonFinalizedCourses();
//    }

    public String UpdateGrades(String studentId, JSONArray grades) {
        for (int i = 0; i < grades.length(); i++) {
            UpdateGrade(studentId, grades.getJSONObject(i));
        }
        return Constants.OK;

    }

    public String UpdateGrade(String studentId, JSONObject grades){
        Student student  = StudentRepoInterface.GetStudent(studentId);
        return UpdateGrade(student, grades);
    }

    public String UpdateGrade(Student student, JSONObject grades) {
        Set<String> updateGradeKeys = Stream.of(Constants.CODE, Constants.GRADE, Constants.TERM).collect(Collectors.toSet());
        Map<String, Object> values = jsonExtractor.Extract(grades, updateGradeKeys);

        try {
            String courseId = values.get(Constants.CODE) + "01";
            Course course = CourseRepoInterface.GetCourse(courseId);
            Float grade = (int) values.get(Constants.GRADE) * 1.0f;
            int term = (int) values.get(Constants.TERM);
            int numOfUnits = course.GetInfo().GetNumOfUnits();
            String name = course.GetInfo().GetName();
            EnrolledCourseInfo ce = new EnrolledCourseInfo(student.GetInfo().GetId(), courseId, term, course.GetInfo().GetNumOfUnits(), grade, course.GetInfo().GetName());
            EnrolledCourse enrolledCourse = new EnrolledCourse(courseId, grade, numOfUnits, term, name);
            repository.insert(ce);
//            student.UpdateGrade(enrolledCourse);
        }catch (SQLException throwables) {
//            throwables.printStackTrace();
        }
        catch(Exception e){
            System.out.println("course doesn't exists please add it");
        }


        return Constants.OK;
    }

    public void ResetWeeklySchedule(String studentId) throws ErrorHandler {
        Student student  = StudentRepoInterface.GetStudent(studentId);
        student.ResetWeeklySchedule();
    }

    public Set<String> GetFinalizedCourses(String studentId) {
        Student student  = StudentRepoInterface.GetStudent(studentId);
        return student.GetFinalizedCourses();
    }

    public List<Course> GetOfferings(String studentId, String filter, String search) {
        Student student  = StudentRepoInterface.GetStudent(studentId);
        student.UpdateSearchFilter(search, filter);
        return student.GetOfferings();
    }

    public Map<String, Map<String, String>> GetPlan(String studentId) {
        Student student  = StudentRepoInterface.GetStudent(studentId);
        return student.GetStudentPlan();
    }


    public void EditWaitList(String command, String studentId, String courseId) throws ErrorHandler, PrerequisitesException, TimeOverlapException, CourseAlreadyAddedToWaitListException, CourseAlreadyAddedException {
        Student student = StudentRepoInterface.GetStudent(studentId);

        if(CourseRepoInterface.GetCourse(courseId) == null) throw new ErrorHandler(Constants.OFFERING_NOT_FOUND);

        if (command.equals(Constants.ADD_TO_WAIT)) {
            student.AddToWaitList(courseId);
        }
        else if (command.equals(Constants.REMOVE_FROM_WAIT)) {
            student.RemoveFromWaitList(courseId);
        }
    }

    public void ResetWaitList(String studentId) {
        Student student = StudentRepoInterface.GetStudent(studentId);
        student.ResetWaitList();
    }

    public void ForceEditWeeklySchedule(String command, String studentId, String courseId) {
        Student student = StudentRepoInterface.GetStudent(studentId);
        student.ForceAddCourseToWeeklySchedule(courseId);
    }
}
