//import HTML.ProfilePage;
//import HTML.CoursesPage;
//import HTML.*;
//import Helper.Constants;
//import Exceptions.*;
//import Logic.Course.Course;
//import Logic.Course.CourseRepoInterface;
//import Logic.Student.Student;
//import Logic.Student.StudentRepoInterface;
//import io.javalin.Javalin;
//
//import java.util.List;
//
//public class Routes {
//    private Javalin app;
//
//    public Routes() {
//        app = Javalin.create().start();
//        SetupGetRoutes();
//        SetupPostRoutes();
//    }
//
//
//
//
//    private void SetupGetRoutes(){
//        app.get("/courses", ctx -> {
//            ctx.html((new CoursesPage()).Render(CourseRepoInterface.GetCourses()));
//        });
//
//        app.get("/profile/:studentId", ctx ->{
//            try {
//                Student student = StudentRepoInterface.GetStudent(ctx.pathParam("studentId"));
//                ctx.html(new ProfilePage().Render(student));
//            } catch (ErrorHandler errorHandler) {
//                ctx.html(new Page404().Render());
//            }
//        });
//        app.get("/course/:courseId/:course_code", ctx ->{
//            try {
//                Course course = CourseRepoInterface.GetCourse(ctx.pathParam("courseId")  + ctx.pathParam("course_code"));
//                ctx.html(new CoursePage().Render(course));
//            } catch (ErrorHandler errorHandler) {
//                ctx.html(new Page404().Render());
//            }
//        });
//        app.get("/change_plan/:studentId", ctx ->{
//            try {
//                String studentId = ctx.pathParam("studentId");
//                Student student = StudentRepoInterface.GetStudent(studentId);
//                List<String> coursesId = student.GetWeeklySchedule();
//                List<Course> courses   = CourseRepoInterface.GetCourses(coursesId);
//                ctx.html(new ChangePlanPage().Render(courses, studentId));
//            } catch (ErrorHandler errorHandler) {
//                ctx.html(new Page404().Render());
//            }
//        });
//
//        app.get("/plan/:studentId", ctx ->{
//            try {
//                Student student = StudentRepoInterface.GetStudent(ctx.pathParam("studentId"));
//                ctx.html(new PlanPage().Render(student));
//            } catch (ErrorHandler errorHandler) {
//                ctx.html(new Page404().Render());
//            }
//        });
//
//        app.get("/submit/:studentId", ctx ->{
//            try {
//                String studentId = ctx.pathParam("studentId");
//                Student student = StudentRepoInterface.GetStudent(studentId);
//                ctx.html(new FinalizePage().Render(student));
//            } catch (ErrorHandler errorHandler) {
//                ctx.html(new Page404().Render());
//            }
//        });
//
//    }
//
//    private void SetupPostRoutes(){
//        app.post("/course/:courseId/:course_code",  ctx ->{
//            Course new_Course = CourseRepoInterface.GetCourse(ctx.pathParam("courseId") + ctx.pathParam("course_code"));
//            try {
//                Student student = StudentRepoInterface.GetStudent(ctx.formParam(Constants.Course_Form_STD_ID));
//                student.AddCourseToWeeklySchedule(new_Course);
//                ctx.html((new CoursesPage()).Render(CourseRepoInterface.GetCourses()));
//            } catch (ErrorHandler errorHandler) {
//                System.out.println(errorHandler.Message());
//                ctx.html(new Page404().Render());
//            } catch (CourseAlreadyAddedException exception) {
//                System.out.println(exception);
//                ctx.html(new CourseAlreadyAddedPage().Render());
//            }
//
//        });
//
//        app.post("/change_plan/:studentId",  ctx ->{
//            try {
//                String studentId = ctx.pathParam("studentId");
//                Student student = StudentRepoInterface.GetStudent(studentId);
//                student.RemoveCourseFromWeeklySchedule(ctx.formParam("course_code") + ctx.formParam("class_code"));
//                List<String> coursesId = student.GetWeeklySchedule();
//                List<Course> courses   = CourseRepoInterface.GetCourses(coursesId);
//                ctx.html(new ChangePlanPage().Render(courses, studentId));
//            } catch (ErrorHandler errorHandler) {
//                System.out.println(errorHandler.Message());
//                ctx.html(new Page404().Render());
//            }
//
//        });
//        app.post("/submit/:studentId", ctx ->{
//            try {
//                String studentId = ctx.pathParam("studentId");
//                Student student = StudentRepoInterface.GetStudent(studentId);
//                student.HandleFinalizeWeeklySchedule();
//                ctx.html(new FinalizedSuccessfulPage().Render());
//            } catch (ErrorHandler errorHandler) {
//                ctx.html(new Page404().Render());
//            } catch (FinalizeException exception) {
//                System.out.println(exception.Message());
//                ctx.html(new FinalizedFailedPage().Render());
//            }
//        });
//    }
//
//    public void Stop(){ app.stop(); }
//
//
//
//
//}
