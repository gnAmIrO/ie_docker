package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course;

import java.sql.SQLException;
import java.util.*;

import ie.ut.ac.ir.Bolbolestan.repository.CourseRepo;
import ie.ut.ac.ir.Bolbolestan.repository.OfferingRepo;
import ie.ut.ac.ir.Bolbolestan.services.model.Exception.ErrorHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.Helper.Constants;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.StudentInterface;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.StudentRepository;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time.ClassTime;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time.ExamTime;
import org.json.simple.JSONObject;


public class Course {
    private CourseInfo info;
    private Integer numOfEnrolledStudents;
    private Set<String> waiters;
    private CourseRepo courseRepo = CourseRepo.getInstance();
//    private OfferingRepo offerRepository = OfferingRepo.getInstance();


    public Course(String name, String id, String groupId, String instructorName, Integer numOfUnits, ClassTime classTime, ExamTime examTime, Integer capacity, String type, Integer numOfEnrolled) {
        this.info                  = new CourseInfo(name, id, groupId, instructorName, numOfUnits, classTime, examTime, capacity, type);
        this.numOfEnrolledStudents = numOfEnrolled;
        this.waiters               = new HashSet<>();
    }

    public Integer GetNumOfEnrolledStudents() {
        return numOfEnrolledStudents;
    }

    public boolean IsCourseFull() {
        return GetNumOfEnrolledStudents().equals(info.GetCapacity()) || info.GetCapacity().equals(0);
    }

    public CourseInfo GetInfo() {
        return info;
    }

    public void AddWaiter(String studentId) {
        waiters.add(studentId);
    }

    public void RemoveWaiter(String studentId) {
        waiters.remove(studentId);
    }

    public void EnrollWaiters() {
        OfferingRepo offerRepository = OfferingRepo.getInstance();
        Set<String> waiters = null; // GetCourseId is bad name it just return Id
        try {
            waiters = offerRepository.FindCoursesWaiters(info.GetId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("EnrollWaiters --> error in geting course Waiters");
        }
        for (String waiter: waiters) {
            EnrollStudent(waiter);
        }

    }

    public void EnrollStudent(String studentId) {
        if (IsCourseFull()) {
            return;
        }
//        if (waiters.contains(studentId)) {
//            RemoveWaiter(studentId);
//        }
        StudentInterface.GetInstance().ForceEditWeeklySchedule(Constants.ADD_TO_WEEKLY_SCHEDULE, studentId, info.GetId());

        ChangeNumOfEnrolledStudents(true);
    }

    public void UnEnrollStudent(String studentId) {
        if (!IsStudentEnrolled(studentId)) {
            return;
        }
        ChangeNumOfEnrolledStudents(false);
    }

    private void ChangeNumOfEnrolledStudents(boolean b) {
        try {
            courseRepo.ChangeNumOfEnrolled(b, info.GetId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("90 --> Error in changing number of enrolled");
        }
    }


    public boolean IsStudentEnrolled(String studentId) {
        try {
            if(StudentRepository.GetInstance().GetStudent(studentId).IsCourseFinalized(info.GetId()))
                return true;
        } catch (ErrorHandler errorHandler) {
            errorHandler.printStackTrace();
        }

        return false;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (!(obj instanceof Course))
            return false;

        Course course = (Course) obj;

        return info.equals(course.GetInfo());
    }

    @Override
    public String toString() {

        return JSONObject.toJSONString(toMap());
    }


    public Map<String, String> toMap(){
        Map<String, String> obj = new LinkedHashMap<>();

        obj.put("code", info.GetId());
        obj.put("name", info.GetName());
        obj.put("Instructor", info.GetInstructorName());

        return obj;
    }
    
    public JSONObject toJson() {
        JSONObject obj = new JSONObject();

        obj.put("info", new JSONObject(this.info.toMap()));
        obj.put("enrolled", this.numOfEnrolledStudents);

        return obj;
    }


    public static int DoCoursesOverlap(Course courseA, Course courseB) {
        int result = 0;
        boolean doClassTimeOverlap = ClassTime.DoesTimeOverlap(courseA, courseB);
        boolean doExamTimeOverlap  = ExamTime.DoesTimeOverlap(courseA, courseB);

        if (doExamTimeOverlap) result += 1;
        if (doClassTimeOverlap) result += 2;

        return result;
    }


    public int GetNumOfWaiters() {
        return waiters.size();
    }
}
