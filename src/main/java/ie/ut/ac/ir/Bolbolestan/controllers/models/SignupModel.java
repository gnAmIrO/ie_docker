package ie.ut.ac.ir.Bolbolestan.controllers.models;

public class SignupModel {
    private String stdId;
    private String email;
    private String password;
    private String firstname;
    private String lastname;
    private String birthDate;
    private String major;
    private String faculty;
    private String grade;


    public String getStdId() {return stdId;}
    public String getPassword() {return password;}
    public String getEmail() {return email;}
    public String getFirstname() {return firstname;}
    public String getLastname() {return lastname;}
    public String getBirthDate() {return birthDate;}
    public String getMajor() {return major;}
    public String getFaculty() {return faculty;}
    public String getGrade() {return grade;}

    public void setStdId(String stdId){ this.stdId = stdId;}
    public void setPassword(String password){ this.password = password;}
    public void setEmail(String email){ this.email = email;}
    public void setFirstname(String firstname){ this.firstname = firstname;}
    public void setLastname(String lastname){ this.lastname = lastname;}
    public void setBirthDate(String birthDate){ this.birthDate = birthDate;}
    public void setMajor(String major){ this.major = major;}
    public void setFaculty(String faculty){ this.faculty = faculty;}
    public void setGrade(String grade){ this.grade = grade;}
}
