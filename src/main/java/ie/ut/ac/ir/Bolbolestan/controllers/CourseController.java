package ie.ut.ac.ir.Bolbolestan.controllers;

import ie.ut.ac.ir.Bolbolestan.controllers.Utilities.DelayManager;
import ie.ut.ac.ir.Bolbolestan.controllers.Utilities.ResponseWrapper;
import ie.ut.ac.ir.Bolbolestan.controllers.models.CoursesModel;
import ie.ut.ac.ir.Bolbolestan.services.model.Exception.*;
import ie.ut.ac.ir.Bolbolestan.services.model.Helper.Constants;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.Course;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.CourseRepoInterface;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.Student;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.StudentInterface;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.StudentRepoInterface;
import ie.ut.ac.ir.Bolbolestan.services.model.SubmitFailedMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CourseController {

    private Integer delay = 0;
    @RequestMapping(value = "/courses", method= RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> GetCourseData(@RequestBody CoursesModel coursesModel) throws JSONException {
        JSONObject courses = new JSONObject();
        String stdId =  SecurityContextHolder.getContext().getAuthentication().getName();
        if(!StudentRepoInterface.DoesStudentExist(stdId)) {
            ResponseWrapper responseWrapper = new ResponseWrapper("not-Found" + stdId,  HttpStatus.UNAUTHORIZED);
            return new ResponseEntity(responseWrapper.toString(), HttpStatus.UNAUTHORIZED);
        }
        List<Course> Courses = StudentInterface.GetInstance().GetOfferings(stdId, coursesModel.getFilter(), coursesModel.getSearch());

        for(Course course : Courses){
            courses.put('C' + course.GetInfo().GetId(), course.toJson());
        }

        DelayManager.Delay(delay);
        return new ResponseEntity(courses.toString(), HttpStatus.OK);
    }


    @RequestMapping(value = "/courses/weeklySchedule", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> GetWeeklySchedule(){
        String stdId =  SecurityContextHolder.getContext().getAuthentication().getName();
        return GetFinalizedCourses(stdId);
    }

    private ResponseEntity GetFinalizedCourses(String stdId) {
        JSONObject courses = new JSONObject();


        Student student = StudentRepoInterface.GetStudent(stdId);
        List<Course> finalizedCourses = CourseRepoInterface.GetCourses(new ArrayList<>(student.GetFinalizedCourses()));
        List<Course> finalizedWaitingCourses =  CourseRepoInterface.GetCourses(new ArrayList<>(student.GetFinalizedWaitingCourses()));

        JSONArray finalizedCoursesList = new JSONArray();
        JSONArray finalizedWaitingCoursesList = new JSONArray();
        System.out.println("printing finalized courses 65 -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        finalizedCourses.forEach(course -> {finalizedCoursesList.put(course.GetInfo().toJson());});
        finalizedWaitingCourses.forEach(course -> {finalizedWaitingCoursesList.put(course.GetInfo().toJson());});

        courses.put("finalizedCourses", finalizedCoursesList);
        courses.put("finalizedWaitingCourses", finalizedWaitingCoursesList);
        courses.put("totalSubmittedCourses", student.GetNumOfSelectedUnits());
        System.out.println(courses.toString());
        return new ResponseEntity(courses.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/submit", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> SubmitCourses(){
        JSONObject response = new JSONObject();
        String stdId =  SecurityContextHolder.getContext().getAuthentication().getName();
        if(!StudentRepoInterface.DoesStudentExist(stdId)) {
            ResponseWrapper responseWrapper = new ResponseWrapper("not-Found" + stdId,  HttpStatus.UNAUTHORIZED);
            return new ResponseEntity(responseWrapper.toString(), HttpStatus.UNAUTHORIZED);
        }
        try {
            StudentInterface.GetInstance().HandleFinalizeWeeklySchedule(stdId);
            response.put("ok", "درس ها با موفقیت ثبت شدند");
            return new ResponseEntity(response.toString(), HttpStatus.OK);
        } catch (FinalizeException e) {
            SubmitFailedMessage.GetInstance().AddMessage(e.Message());
        } catch (ErrorHandler errorHandler) {
            SubmitFailedMessage.GetInstance().AddMessage(errorHandler.Message());
        }
        response.put("error", SubmitFailedMessage.GetInstance().toString());
        return ResponseEntity.ok(response.toString());
    }

    @RequestMapping(value = "/courses/{course}", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> PostNewCourse(@PathVariable String course, @RequestBody CoursesModel coursesModel) { // TODO: do we need to check if student exists? doubt it
        JSONObject response = new JSONObject();
        String stdId =  SecurityContextHolder.getContext().getAuthentication().getName();

        if(!StudentRepoInterface.DoesStudentExist(stdId)) {
            ResponseWrapper responseWrapper = new ResponseWrapper("not-Found" + stdId,  HttpStatus.UNAUTHORIZED);
            return new ResponseEntity(responseWrapper.toString(), HttpStatus.UNAUTHORIZED);
        }
        System.out.println(coursesModel.getStatus());
        try {
            if(coursesModel.getStatus().equals("0")) {
                StudentInterface.GetInstance().EditWeeklySchedule(Constants.ADD_TO_WEEKLY_SCHEDULE, stdId, course);
            }else if(coursesModel.getStatus().equals("1")) {
                StudentInterface.GetInstance().EditWaitList(Constants.ADD_TO_WAIT, stdId, course);
            }
            response.put("ok", "ok");
        }catch (CourseAlreadyAddedToWaitListException e) {
            response.put("error", "درس رو قبلا اضافه کردی به انتظار " + course);
        } catch (CourseAlreadyAddedException e) {
            response.put("error", "درس رو گرفتی " + course);
        } catch (StudentNotEnrolledException e) {
            response.put("error", "دانش آموز ثبت نشده");
        } catch (PrerequisitesException e) {
            response.put("error", "پیش نیازش رو پاس نکردی " + course);
        } catch (ErrorHandler errorHandler) {
            response.put("ok", "ok");
        } catch (TimeOverlapException e) {
            response.put("error", "زمان هات رو چک کن " + course);
        }

        DelayManager.Delay(delay);
        return new ResponseEntity(response.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/{course}", method=RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> DeleteSelectedOffering(@PathVariable String course){
        JSONObject response = new JSONObject();
        String stdId =  SecurityContextHolder.getContext().getAuthentication().getName();

        if(!StudentRepoInterface.DoesStudentExist(stdId)) {
            ResponseWrapper responseWrapper = new ResponseWrapper("not-Found" + stdId,  HttpStatus.UNAUTHORIZED);
            return new ResponseEntity(responseWrapper.toString(), HttpStatus.UNAUTHORIZED);
        }
        try {
            StudentInterface.GetInstance().EditWaitList(Constants.REMOVE_FROM_WAIT, stdId, course);
            StudentInterface.GetInstance().EditWeeklySchedule(Constants.REMOVE_FROM_WEEKLY_SCHEDULE, stdId, course);

            response.put("ok", "ok");
        }catch (CourseAlreadyAddedToWaitListException e) {
            response.put("error", "درس رو قبلا اضافه کردی به انتظار " + course);
        } catch (CourseAlreadyAddedException e) {
            response.put("error", "درس رو گرفتی " + course);
        } catch (StudentNotEnrolledException e) {
            response.put("error", "دانش آموز ثبت نشده");
        } catch (PrerequisitesException e) {
            response.put("error", "پیش نیازش رو پاس نکردی " + course);
        } catch (ErrorHandler errorHandler) {
            response.put("ok", "ok");
        } catch (TimeOverlapException e) {
            response.put("error", "زمان هات رو چک کن " + course);
        }

        DelayManager.Delay(delay);
        return new ResponseEntity(response.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses", method=RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> ResetWeeklySchedule(){
        JSONObject response = new JSONObject();

        String stdId =  SecurityContextHolder.getContext().getAuthentication().getName();
        if(!StudentRepoInterface.DoesStudentExist(stdId)) {
            ResponseWrapper responseWrapper = new ResponseWrapper("not-Found" + stdId,  HttpStatus.UNAUTHORIZED);
            return new ResponseEntity(responseWrapper.toString(), HttpStatus.UNAUTHORIZED);
        }

        try {
            StudentInterface.GetInstance().ResetWaitList(stdId);
            StudentInterface.GetInstance().ResetWeeklySchedule(stdId);
//            return GetFinalizedCourses(stdId);
            response.put("ok", "ok");
        } catch (ErrorHandler errorHandler) {
            response.put("error", "ErrorHandler");
        }
        DelayManager.Delay(delay);
        return new ResponseEntity(response.toString(), HttpStatus.OK);
    }
}
