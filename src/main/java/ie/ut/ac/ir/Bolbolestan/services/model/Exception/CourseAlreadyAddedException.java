package ie.ut.ac.ir.Bolbolestan.services.model.Exception;

public class CourseAlreadyAddedException extends Throwable {
    public CourseAlreadyAddedException(String errorMessage) {
        super(errorMessage);
    }
}
