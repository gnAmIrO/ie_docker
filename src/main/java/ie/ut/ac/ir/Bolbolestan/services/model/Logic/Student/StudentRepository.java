package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student;

import ie.ut.ac.ir.Bolbolestan.repository.StudentRepo;
import ie.ut.ac.ir.Bolbolestan.services.model.Exception.ErrorHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.Helper.Constants;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class StudentRepository {
    private static Map<String, Student> students;
    private StudentRepo repository = StudentRepo.getInstance();
    private static StudentRepository instance;
    static {
        students = new HashMap<>();
    }

    public static StudentRepository GetInstance(){
        if (instance == null) {
            instance = new StudentRepository();
        }
        return instance;
    }

    public void AddStudent(String name, String lastName, String id, String entryYear, String birthDateText, String field, String faculty, String level, String status, String img, String email, String password) throws ErrorHandler, SQLException {
        if(DoesStudentExist(id)) throw new ErrorHandler(Constants.STUDENT_ALREADY_EXISTS);

        Student student = new Student(name, lastName, id, entryYear, birthDateText, field, faculty, level, status, img, email, password);
        student.GetInfo().HashPassword();
        repository.insert(student);
        students.put(id, student);
    }

    public boolean DoesStudentExist(String studentId) {
        try {
            return repository.FindStudent(studentId, false) != null;
        } catch (SQLException throwables) {
            return false;
        }
    }

    public Student GetStudent(String id) {
        try {
            return repository.FindStudent(id, false);
        } catch (SQLException throwables) {
            return null;
        }
    }


}
