package ie.ut.ac.ir.Bolbolestan.repository;

import ie.ut.ac.ir.Bolbolestan.repository.helper.EnrolledNumChanger;
import ie.ut.ac.ir.Bolbolestan.repository.helper.SearchFilter;
import ie.ut.ac.ir.Bolbolestan.services.model.Helper.Constants;
import ie.ut.ac.ir.Bolbolestan.services.model.JSON.JsonHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.Course;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.CourseInfo;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time.ClassTime;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time.ExamTime;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class CourseRepo extends Repository<Course, String>{

    private static final String TABLE_NAME = "course";
    private static CourseRepo instance;

    public static CourseRepo getInstance(){
        if (instance == null) {
            try {
                instance = new CourseRepo();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in CourseRepository.create query.");
            }
        }
        return instance;
    }

    private CourseRepo() throws SQLException{
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s(id CHAR(50),\n name CHAR(100),\n code CHAR(100),\ngroupId CHAR(100),\ninstructorName CHAR(80),\nnumOfUnits INT,\nclassTimeDays CHAR(100),\nclassTimeIn TIME,\nclassTimeOut TIME,\nExamTime CHAR(100),\ncapacity INT,\nnumOfEnrolled INT,\ntype CHAR(100), PRIMARY KEY(id));", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }

    public List<Course> getOfferings(String searchFilter, String typeFilter) throws SQLException { // TODO:

        SearchFilter filter = new SearchFilter(typeFilter, searchFilter);
        return findAll(filter);
    }

    @Override
    protected String getTransactionStatement(Object obj) {
        return String.format(
            "Start TRANSACTION; " +
            "UPDATE %s SET numOfEnrolled = course.numOfEnrolled + ? WHERE id = ?;" +
            "COMMIT;", TABLE_NAME);
    }

    @Override
    protected void fillTransactionValues(PreparedStatement st, Object obj) throws SQLException {
        EnrolledNumChanger changer = (EnrolledNumChanger) obj;

        st.setInt(1, changer.IsIncrease() ? 1 : -1);
        st.setString(2, changer.GetCourseId());
    }


    public void ChangeNumOfEnrolled(boolean isIncrease, String courseId) throws SQLException {
        EnrolledNumChanger changer = new EnrolledNumChanger(isIncrease, courseId);
        transact(changer);
    }

    @Override
    protected String getFindByIdStatement(Object obj) {
        return String.format("SELECT * FROM %s course WHERE course.id = ?;", TABLE_NAME);
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, Object obj) throws SQLException {
        String id = (String) obj;
        st.setString(1, id);
    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s (id, name, code, groupId, instructorName, numOfUnits, classTimeDays, classTimeIn, classTimeOut, ExamTime, capacity, numOfEnrolled, type) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", TABLE_NAME);
    }

    public List<Course> getFullCourses() throws SQLException {
        String stmt = String.format("SELECT * FROM %s course WHERE course.capacity = course.numOfEnrolled;", TABLE_NAME);
        return findAll(stmt);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Course course) throws SQLException {
        CourseInfo courseInfo = course.GetInfo();


        st.setString(Constants.COURSEDB_ID_INDEX, courseInfo.GetId()); // 1
        st.setString(Constants.COURSEDB_NAME_INDEX, courseInfo.GetName()); // 2
        st.setString(Constants.COURSEDB_CLASSID_INDEX, courseInfo.GetClassId()); // 3
        st.setString(Constants.COURSEDB_GROUPID_INDEX, courseInfo.GetGroupId()); // 4
        st.setString(Constants.COURSEDB_INSTRUCTORNAME_INDEX, courseInfo.GetInstructorName()); // 5
        st.setInt(Constants.COURSEDB_NUMOFUNITS_INDEX, courseInfo.GetNumOfUnits()); // 6
        st.setString(Constants.COURSEDB_CLASSTIMEDAYS_INDEX, String.join(" ", courseInfo.GetClassTime().GetDays())); // 7
        st.setTime(Constants.COURSEDB_CLASSTIME_IN_INDEX, courseInfo.GetClassTime().GetBeginningTime()); // 8
        st.setTime(Constants.COURSEDB_CLASSTIME_OUT_INDEX, courseInfo.GetClassTime().GetEndTime()); // 9
        st.setString(Constants.COURSEDB_EXAMTIME_INDEX, courseInfo.GetExamTime().toString()); // 10
        st.setInt(Constants.COURSEDB_CAPACITY_INDEX, courseInfo.GetCapacity()); // 11
        st.setInt(Constants.COURSEDB_NUMOFENROLLED_INDEX, 0); // 12
        st.setString(Constants.COURSEDB_TYPE_INDEX, courseInfo.GetType()); // 13

    }

    @Override
    protected String getFindAllStatement(Object obj) {
        SearchFilter searchFilter = (SearchFilter)obj;
        String typeQuery = String.format("AND course.type = ?");
        if (searchFilter.GetTypeFilter().equals("All")) {
            typeQuery = "";
        }
        String stmt = String.format("SELECT * FROM %s course  WHERE name LIKE ? %s;", TABLE_NAME, typeQuery);
        return stmt;
    }

    @Override
    protected void fillFindAllValues(PreparedStatement st, Object obj) throws SQLException {
        SearchFilter filter = (SearchFilter) obj;
        st.setString(1, "%" + filter.GetSearchInput() + "%");
        if (!filter.GetTypeFilter().equals("All")) {
            st.setString(2, filter.GetTypeFilter());
        }
    }


    @Override
    protected Course convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        ClassTime classTime = new ClassTime(Arrays.asList(rs.getString(Constants.COURSEDB_CLASSTIMEDAYS_INDEX).split(" ")),
                rs.getTime(Constants.COURSEDB_CLASSTIME_IN_INDEX),
                rs.getTime(Constants.COURSEDB_CLASSTIME_OUT_INDEX));

        JSONObject jsonExamTime = JsonHandler.DeserializeJsonObject(rs.getString(Constants.COURSEDB_EXAMTIME_INDEX));


        Course course = new Course(rs.getString(Constants.COURSEDB_NAME_INDEX),
                rs.getString(Constants.COURSEDB_CLASSID_INDEX),
                rs.getString(Constants.COURSEDB_GROUPID_INDEX),
                rs.getString(Constants.COURSEDB_INSTRUCTORNAME_INDEX),
                rs.getInt(Constants.COURSEDB_NUMOFUNITS_INDEX),
                classTime,
                ExamTime.ConvertToClass(jsonExamTime),
                rs.getInt(Constants.COURSEDB_CAPACITY_INDEX),
                rs.getString(Constants.COURSEDB_TYPE_INDEX),
                rs.getInt(Constants.COURSEDB_NUMOFENROLLED_INDEX));

        return course;
    }

    @Override
    public ArrayList<Course> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<Course> courses = new ArrayList<>();
        while (rs.next()) {
//            System.out.println(rs);
            courses.add(this.convertResultSetToDomainModel(rs));
        }
        return courses;
    }




}
