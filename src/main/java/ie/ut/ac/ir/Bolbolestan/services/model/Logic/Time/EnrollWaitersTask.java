package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time;

import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.CourseRepository;

import java.util.TimerTask;


public class EnrollWaitersTask extends TimerTask {

    @Override
    public void run() {
        System.out.println("Job triggered by scheduler");
        CourseRepository.GetInstance().EnrollWaiters();
    }
}