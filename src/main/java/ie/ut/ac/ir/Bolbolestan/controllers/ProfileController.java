package ie.ut.ac.ir.Bolbolestan.controllers;

import ie.ut.ac.ir.Bolbolestan.controllers.Utilities.ResponseWrapper;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.Student;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.StudentRepoInterface;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;


@RestController
public class ProfileController {


    private JSONObject CreateResponseData(String stdId) {
        Student student = StudentRepoInterface.GetStudent(stdId);
        System.out.println(stdId);
        System.out.println(student.toJson().toString());
        return student.toJson();
    }

    @RequestMapping(value = "/profile", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> GetProfileData(){
        String stdId =  SecurityContextHolder.getContext().getAuthentication().getName();
        System.out.println("28 Profile Controller: " + stdId);
//        ResponseWrapper responseWrapper;
        System.out.println(stdId);

        return new ResponseEntity(CreateResponseData(stdId).toString(), HttpStatus.OK);

    }


}
