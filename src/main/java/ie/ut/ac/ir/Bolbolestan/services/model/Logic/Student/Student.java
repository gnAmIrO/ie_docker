package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student;

//import javafx.beans.binding.MapExpression;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.EnrolledCourse;
import ie.ut.ac.ir.Bolbolestan.services.model.Exception.*;
import ie.ut.ac.ir.Bolbolestan.services.model.Helper.Constants;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.Course;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.CourseRepoInterface;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time.ClassTime;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.WaitList;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.WeeklySchedule;
import org.json.JSONObject;

import java.util.*;


public class Student {
    private StudentInfo    info;
    private WeeklySchedule weeklySchedule;
    private WaitList       waitList;

    public String GetSearchFilter() {
        return searchFilter;
    }

    public String GetTypeFilter() {
        return typeFilter;
    }

    private String         searchFilter;
    private String         typeFilter;


    public Student(String name, String lastName, String id, String entryYear, String birthDateText, String field, String faculty, String level, String status, String img, String email, String password) {
        this.info = new StudentInfo(name, lastName, id, entryYear, birthDateText, field, faculty, level, status, img, email, password);
        this.weeklySchedule = new WeeklySchedule(this);
        this.waitList       = new WaitList(this);
        this.searchFilter   = "";
        this.typeFilter = "";
    }

    public StudentInfo GetInfo() {
        return info;
    }

    public boolean HasPassedAllPrerequisites(Set<String> prerequisites) {
        return info.HasPassedAllPrerequisites(prerequisites);
    }

    public List<Course> GetOfferings() {
        return CourseRepoInterface.GetAllCourses(GetSearchFilter(), typeFilter);
    }


    public Integer GetNumOfSelectedUnits() {
        return weeklySchedule.GetNumOfSelectedUnits();
    }

    public boolean IsNumOfSelectedUnitsOverCapacity(Integer numOfSelectedUnits) {
        return weeklySchedule.IsNumOfSelectedUnitsOverCapacity(numOfSelectedUnits);
    }

    public boolean IsNumOfSelectedUnitsUnderCapacity(Integer numOfSelectedUnits) {
        return weeklySchedule.IsNumOfSelectedUnitsUnderCapacity(numOfSelectedUnits);
    }

    public boolean IsNumOfSelectedUnitsValid(Integer numOfSelectedUnits) {
        return weeklySchedule.IsNumOfSelectedUnitsValid(numOfSelectedUnits);
    }

    public boolean DoesCourseExistInWeeklySchedule(String courseId) {
        return weeklySchedule.DoesCourseExistInWeeklySchedule(courseId);
    }

    public List<String> GetWeeklySchedule() { return weeklySchedule.GetWeeklySchedule(); }

    public boolean IsCourseFinalized(String courseId) throws ErrorHandler {
        return weeklySchedule.IsCourseFinalized(courseId);
    }

    public void AddCourseToWeeklySchedule(String courseId) throws CourseAlreadyAddedException, PrerequisitesException, TimeOverlapException {
        weeklySchedule.AddCourseToWeeklySchedule(courseId);
        if (waitList.DoesExist(courseId)) {
            waitList.Remove(courseId);
        }
    }

    public void ForceAddCourseToWeeklySchedule(String courseId)  {
        weeklySchedule.ForceAddCourseToWeeklySchedule(courseId);
        if (waitList.DoesExist(courseId)) {
            waitList.Remove(courseId);
        }
    }

    public void AddCourseToWeeklySchedule(Course course) throws CourseAlreadyAddedException, PrerequisitesException, TimeOverlapException {
        weeklySchedule.AddCourseToWeeklySchedule(course.GetInfo().GetId());
    }

    public void RemoveCourseFromWeeklySchedule(String courseId) throws ErrorHandler {
        if (weeklySchedule.DoesCourseExistInWeeklySchedule(courseId)) {
//            CourseRepoInterface.UnEnrollStudent(info.GetId(), courseId);

        }
        weeklySchedule.RemoveCourseFromWeeklySchedule(courseId);
    }


    public void HandleFinalizeWeeklySchedule() throws FinalizeException, ErrorHandler {
        Set<String> courses = new HashSet<>(weeklySchedule.GetWeeklySchedule());
        Set<String> coursesToCheckForCapacity = new HashSet<>(weeklySchedule.GetWeeklySchedule());
        coursesToCheckForCapacity.removeAll(weeklySchedule.GetFinalizedCourses());

        List<Course> fullCourses = CourseRepoInterface.GetFullCourses(coursesToCheckForCapacity);
        Integer numOfUnits = GetNumOfSelectedUnits();

        if (!weeklySchedule.ShouldFinalizeWeeklySchedule(fullCourses)) {
            weeklySchedule.ResetWeeklySchedule();
            waitList.Reset();
            throw new FinalizeException(fullCourses,
                    IsNumOfSelectedUnitsOverCapacity(numOfUnits),
                    IsNumOfSelectedUnitsUnderCapacity(numOfUnits));
        }
        else {
            CourseRepoInterface.UnEnrollStudent(info.GetId(), weeklySchedule.GetRemovedCourses());
            weeklySchedule.HandleFinalizeWeeklySchedule();

            CourseRepoInterface.EnrollStudent(info.GetId(), courses);



//            CourseRepoInterface.AddStudentToWaitLists(info.GetId(), waitList.GetList());
//            CourseRepoInterface.RemoveStudentFromCoursesWaitList(info.GetId(), waitList.GetRemovedCourses());
            waitList.Finalize();
        }


    }

    public void ResetWeeklySchedule() {
        weeklySchedule.ResetWeeklySchedule();
    }




    public Map<String, Map<String, String>> GetStudentPlan(){
        Map<String, Map<String, String>> weeklyPlan = new HashMap<>();
        for(String day : Constants.DAYS){
            Map<String, String> timeTemp = new HashMap<>();
            for(String time: Constants.TIMES){
                timeTemp.put(time, "");
                weeklyPlan.put(day, timeTemp);
            }
        }

        Set<String> studentWeeklySchedule = GetFinalizedCourses();
        for(String courseId : studentWeeklySchedule){
            Course course = CourseRepoInterface.GetCourse(courseId);
            ClassTime courseClassTime = course.GetInfo().GetClassTime();
            for(String classDay : courseClassTime.GetDays()){
                weeklyPlan.get(classDay).put(courseClassTime.GetTimeAsString(), course.GetInfo().GetName());
            }
        }

        return weeklyPlan;
    }



    public Set<String> GetFinalizedCourses() {
        return weeklySchedule.GetFinalizedCourses();
    }
    public Set<String> GetFinalizedWaitingCourses(){ return waitList.GetFinalizedCourses();}



    public void UpdateSearchFilter(String searchFilter, String filter){ this.searchFilter = searchFilter; this.typeFilter = filter; }

    public boolean HasOverlap(String courseId) {
        Set<String> coursesToCheckForOverlapping = new HashSet<>();
//        coursesToCheckForOverlapping.add(courseId);
        coursesToCheckForOverlapping.addAll(weeklySchedule.GetFinalizedCourses());
        coursesToCheckForOverlapping.addAll(weeklySchedule.GetWeeklySchedule());
        coursesToCheckForOverlapping.addAll(waitList.GetList());

        return !CourseRepoInterface.GetOverlappingCourses(coursesToCheckForOverlapping, courseId).isEmpty();
    }

    public void AddToWaitList(String courseId) throws CourseAlreadyAddedToWaitListException, CourseAlreadyAddedException, PrerequisitesException, TimeOverlapException {
        if (waitList.DoesExist(courseId)) {
            throw new CourseAlreadyAddedToWaitListException("CourseAlreadyAddedToWaitList");
        }
        if (weeklySchedule.DoesCourseExistInWeeklySchedule(courseId) || weeklySchedule.DoesCourseExistInFinalizedSchedule(courseId)) {
            throw new CourseAlreadyAddedException("CourseAlreadyAdded");
        }
        Course course = CourseRepoInterface.GetCourse(courseId);
        if (!HasPassedAllPrerequisites(course.GetInfo().GetPrerequisites())) {
            throw new PrerequisitesException("Prerequisites not passed");
        }

        if (HasOverlap(courseId)) {
            throw new TimeOverlapException(Constants.OVERLAP_IN_SCHEDULE);
        }
        waitList.Add(courseId);
    }

    public void RemoveFromWaitList(String courseId)  {
        if (!waitList.DoesExist(courseId)) {
            return;
        }
        waitList.Remove(courseId);
    }

    public void ResetWaitList() {
        waitList.Reset();
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == this)
            return true;

        if (!(obj instanceof Student))
            return false;

        Student otherStudent = (Student) obj;

        return info.equals(otherStudent.GetInfo());
    }


    public JSONObject toJson() {
        return GetInfo().toJson();
    }
}
