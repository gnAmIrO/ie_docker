package ie.ut.ac.ir.Bolbolestan.services.model.HTTP;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import javax.swing.text.html.parser.Entity;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class RequestCommand {

    private String responseBody;
    private int    status;

    public RequestCommand() {
        responseBody = null;
        status       = 0;
    }

    public void ExecutePostRequest(String address, JSONObject requestBody){
        try (CloseableHttpClient client = HttpClientBuilder.create().build()) {
            var request = new HttpPost(address);
            request.setHeader("User-Agent", "Java client");
            request.addHeader("content-type", "application/json");
            request.addHeader("Accept", "application/json");
            request.setEntity(new StringEntity(requestBody.toString()));

            HttpResponse response = client.execute(request);

            var bufReader = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));

            status = response.getStatusLine().getStatusCode();
            System.out.println("45 -->> " + status);
            var builder = new StringBuilder();

            String line;

            while ((line = bufReader.readLine()) != null) {

                builder.append(line);
                builder.append(System.lineSeparator());
            }
            responseBody = builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void ExecuteGetRequest(String address) {
        CloseableHttpClient client = HttpClients.createDefault();
        try {
            HttpGet request = new HttpGet(address);
            CloseableHttpResponse response = client.execute(request);
            try {
                status = response.getStatusLine().getStatusCode();

                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    responseBody = EntityUtils.toString(entity);
                }
            }
            finally {
                response.close();
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String GetResponse() {
        if (status != 0) {
            return responseBody;
        }
        return "";
    }
}
