package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import ie.ut.ac.ir.Bolbolestan.services.model.JSON.JsonHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.Course;
import org.json.simple.JSONArray;
import org.json.JSONObject;



public class ClassTime {
    private List<String> days;
    private Interval time;
    private String timeInString;
    private Time beginTime, endTime;
    private Long beginInEpochs, endInEpochs;

    public ClassTime(List<String> days, String time) {
        this.days = new ArrayList<String>();
        this.timeInString = time;
        for (String day : days) { // Since in test cases we had Monday and saturday(uppercase and lowercase) days.
            this.days.add(day.toLowerCase(Locale.ROOT));
        }

        String[] strings = time.split("-");
        DateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            Date date = sdf.parse(strings[0]);
            beginTime = new Time(date.getTime());
            date = sdf.parse(strings[1]);
            endTime = new Time(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        beginInString = strings[0];
//        endInString   = strings[1];
//        beginInEpochs = ;
//        endInEpochs   = ConvertHourToEpochs(endInString);
        this.time = new Interval(ConvertHourToEpochs(strings[0]), ConvertHourToEpochs(strings[1]));
    }

    public ClassTime(List<String> days, Time timeIn, Time timeOut) {
        this(days, timeIn.toString().substring(0, timeIn.toString().length() - 3) + "-" + timeOut.toString().substring(0, timeOut.toString().length() - 3));
    }

    public Time GetBeginningTime() {
        return beginTime;
    }

    public Time GetEndTime() {
        return endTime;
    }

    public Long GetBeginningTimeInEpochs() {
        return beginInEpochs;
    }

    public Long GetEndTimeInEpochs() {
        return endInEpochs;
    }

    public long ConvertHourToEpochs(String time) {
        long result;
        String pattern = "H";
        if (time.startsWith("0")) {
            pattern += "H";
        }
        if (time.contains(":")) {
            pattern += ":mm";
        }


        LocalTime localTime = LocalTime.parse(time, DateTimeFormatter.ofPattern(pattern));
        result = localTime.getHour() * 60 + localTime.getMinute();
        return result;
    }

    public List<String> GetDays(){ return days; }

    public String GetTimeAsString(){ return timeInString; }

    public Interval GetTime(){ return time; }

    public static boolean DoesTimeOverlap(Course courseA, Course courseB) {
        return ClassTime.DoesTimeOverlap(courseA.GetInfo().GetClassTime(), courseB.GetInfo().GetClassTime());
    }

    public static boolean DoesTimeOverlap(ClassTime classTimeA, ClassTime classTimeB) {
        List<String> courseADays = classTimeA.GetDays();
        Interval     courseATime = classTimeA.GetTime();

        List<String> courseBDays = classTimeB.GetDays();
        Interval     courseBTime = classTimeB.GetTime();

        for(String day: courseADays)
            if(courseBDays.contains(day))
                if (Interval.HasOverlap(courseATime, courseBTime))
                    return true;
        return false;
    }



    @Override
    public String toString() {
        JSONObject classTimeJson = new JSONObject();

        classTimeJson.put("days", JsonHandler.DeserializeJsonArray(JSONArray.toJSONString(days)));
        classTimeJson.put("time", timeInString);

        return JsonHandler.SerializeJsonData(classTimeJson);
    }

//    public static ClassTime ConvertToClass(JSONObject classTimeObject){
//        return new ClassTime(Arrays.asList(classTimeObject.get("days").toString().split(",")), classTimeObject.getString("time"));
//    }

    public JSONObject toJson() {
        JSONObject classTimeJson = new JSONObject();

        classTimeJson.put("days", JsonHandler.DeserializeJsonArray(JSONArray.toJSONString(days)));
        List<String> times = Arrays.asList(timeInString.split("-"));
        classTimeJson.put("time", time.toJson(times.get(0), times.get(1)));

        return classTimeJson;
    }
}
