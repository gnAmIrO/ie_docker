package ie.ut.ac.ir.Bolbolestan.repository.helper;

public enum OfferCommandType {
    RESET,
    DELETE,
    SUBMIT,
    ADD,
    FORCE_ADD,
    FIND_WAITERS,
    FIND_REMOVED,
    FIND_WAITED,
    FIND_BY_STDID,
    FIND_SAME_BY_STDID
}
