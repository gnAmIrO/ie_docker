package ie.ut.ac.ir.Bolbolestan.controllers.models;

public class LoginModel {
    private String email;
    private String password;
    private String token;


    public String getEmail() {return email;}
    public String getPassword() {return password;}
    public void setEmail(String email){ this.email = email;}
    public void setPassword(String password){ this.password = password;}
    public void setToken(String token){ this.token = token;}
}
