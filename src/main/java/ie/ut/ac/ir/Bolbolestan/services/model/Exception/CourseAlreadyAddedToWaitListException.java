package ie.ut.ac.ir.Bolbolestan.services.model.Exception;

public class CourseAlreadyAddedToWaitListException extends Throwable {
    public CourseAlreadyAddedToWaitListException(String errorMessage) {
        super(errorMessage);
    }
}
