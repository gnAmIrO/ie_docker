package ie.ut.ac.ir.Bolbolestan;

import ie.ut.ac.ir.Bolbolestan.security.JWTAuthorizationFilter;
import ie.ut.ac.ir.Bolbolestan.services.model.API.APIHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.Exception.ErrorHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.JSON.JsonHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.CourseRepoInterface;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.StudentRepoInterface;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time.EnrollWaitersTask;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.sql.SQLException;
import java.util.Timer;

@SpringBootApplication
public class BolbolestanApplication {

    public static void main(String[] args) {

        APIHandler apiHandler = new APIHandler();
        JSONArray courses  = JsonHandler.DeserializeJsonArray(apiHandler.RequestToAPI("http://138.197.181.131:5200/api/courses"));
        JSONArray students = JsonHandler.DeserializeJsonArray(apiHandler.RequestToAPI("http://138.197.181.131:5200/api/students"));
        try {
            CourseRepoInterface.AddNewCourses(courses);
            StudentRepoInterface.AddNewStudents(students);
        }
        catch (ErrorHandler | SQLException errorHandler) {
            errorHandler.printStackTrace();
            // System.out.println(errorHandler);
        }
        Timer timer = new Timer();
        EnrollWaitersTask task = new EnrollWaitersTask();
        timer.schedule(task, 0, 15000);
        SpringApplication.run(BolbolestanApplication.class, args);
    }

    @EnableWebSecurity
    @Configuration
    class WebSecurityConfig extends WebSecurityConfigurerAdapter {
        @Autowired
        private JWTAuthorizationFilter jwtAuthorizationFilter;
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable()
                    .authorizeRequests()
                    .antMatchers("/auth/login/**").permitAll()
                    .antMatchers("/auth/signup").permitAll()
                    .antMatchers("/profile").hasRole("USER")
                    .antMatchers("/login/reset").hasRole("USER")
                    .antMatchers("/courses/*").hasRole("USER")
                    .antMatchers("/weekly_schedule").hasRole("USER")
                    .anyRequest().authenticated()
                    .and().sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                    .and()
                    .addFilterBefore(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class);
        }
    }


}
