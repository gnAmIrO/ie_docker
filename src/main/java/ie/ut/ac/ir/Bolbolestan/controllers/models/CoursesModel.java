package ie.ut.ac.ir.Bolbolestan.controllers.models;

public class CoursesModel {
    private String id;
    private String search;
    private String filter;
    private String status;


    public String getId() {return id;}
    public String getSearch() {return search;}
    public String getFilter() {return filter;}
    public String getStatus() {return status;}
    public void setId(String id){ this.id = id;}
    public void setSearch(String search){ this.search = search;}
    public void setFilter(String filter){ this.filter = filter;}
    public void setStatus(String status){ this.status = status;}
}
