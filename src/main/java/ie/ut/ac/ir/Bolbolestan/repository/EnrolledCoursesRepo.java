package ie.ut.ac.ir.Bolbolestan.repository;

import ie.ut.ac.ir.Bolbolestan.model.EnrolledCourseInfo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EnrolledCoursesRepo extends Repository<EnrolledCourseInfo, String> {

    private static final String TABLE_NAME = "CourseEnrolled";
    private static EnrolledCoursesRepo instance;

    public static EnrolledCoursesRepo getInstance() {
        if (instance == null) {
            try {
                instance = new EnrolledCoursesRepo();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in CourseEnrolledRepository.create query.");
            }
        }
        return instance;
    }

    private EnrolledCoursesRepo() throws SQLException {

        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s(id MEDIUMINT NOT NULL AUTO_INCREMENT,\n" +
                        "                        term INT,\n" +
                        "                        student_number_id CHAR(100),\n" +
                        "                        courseName CHAR(100),\n" +
                        "                        unit INT,\n" +
                        "                        grade FLOAT,\n" +
                        "                        course_id CHAR(100),\n" +
                        "                        UNIQUE(student_number_id, course_id),\n" +
                        "                        PRIMARY KEY(id),\n" +
                        "                        FOREIGN KEY (student_number_id) REFERENCES student(id) ON DELETE CASCADE,\n" +
                        "                        FOREIGN KEY (course_id) REFERENCES course(id) ON DELETE CASCADE);", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }



//    protected String getFindByTermStatement() {
//        return null;
////        return String.format("SELECT * FROM %s courseEnrolled WHERE courseEnrolled.term = ?;", TABLE_NAME);
//    }



    public List<EnrolledCourseInfo> GetEnrolledCoursesInfo(String stdId) throws SQLException {
        return findAll((Object) stdId);
    }


    @Override
    protected String getFindByIdStatement(Object obj) {
        return null;
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, Object obj) throws SQLException {
        String id = (String) obj;
        st.setString(1, id);
    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s (term, student_number_id, courseName, unit, grade, course_id) VALUES(?, ?, ?, ?, ?, ?)", TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, EnrolledCourseInfo ce) throws SQLException {
        st.setInt(1, ce.GetTerm());
        st.setString(2, ce.GetStudentNumberId());
        st.setString(3, ce.GetCourseName());
        st.setInt(4, ce.GetUnit());
        st.setFloat(5, ce.GetGrade());
        st.setString(6, ce.GetCourseId());
    }

    @Override
    protected String getFindAllStatement(Object obj) {
        return String.format("SELECT * FROM %s courseEnrolled WHERE courseEnrolled.student_number_id = ?;", TABLE_NAME);
    }

    @Override
    protected void fillFindAllValues(PreparedStatement st, Object obj) throws SQLException {
        String stdId = (String) obj;
        st.setString(1, stdId);

    }

    @Override
    protected EnrolledCourseInfo convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        EnrolledCourseInfo std = new EnrolledCourseInfo(rs.getString(3), rs.getString(4), rs.getInt(2), rs.getInt(5), rs.getFloat(6), rs.getString(7));
        return std;
    }

    @Override
    protected ArrayList<EnrolledCourseInfo> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<EnrolledCourseInfo> std = new ArrayList<>();
        while (rs.next()) {
            std.add(this.convertResultSetToDomainModel(rs));
        }
        return std;
    }

    @Override
    protected void fillTransactionValues(PreparedStatement preparedStatement, Object obj) throws SQLException {

    }

    @Override
    protected String getTransactionStatement(Object obj) {
        return null;
    }
}
