package ie.ut.ac.ir.Bolbolestan.controllers.Utilities;

import java.util.concurrent.TimeUnit;

public class DelayManager {

    public static void Delay(Integer timeout){
        try {
            TimeUnit.SECONDS.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
