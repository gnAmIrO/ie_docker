package ie.ut.ac.ir.Bolbolestan.repository;

import ie.ut.ac.ir.Bolbolestan.model.Offering;
import ie.ut.ac.ir.Bolbolestan.repository.helper.OfferCommandInfo;
import ie.ut.ac.ir.Bolbolestan.repository.helper.OfferCommandType;
import ie.ut.ac.ir.Bolbolestan.repository.helper.OfferInfo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class OfferingRepo extends Repository<Offering, String>{
    private static final String TABLE_NAME = "offering";
    private static OfferingRepo instance;

    public static OfferingRepo getInstance() {
        if (instance == null) {
            try {
                instance = new OfferingRepo();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in OfferingRepository.create query.");
            }
        }
        return instance;
    }


    private OfferingRepo() throws SQLException {

        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s(id MEDIUMINT NOT NULL AUTO_INCREMENT,\n" +
                        "                        student_id CHAR(100),\n" +
                        "                        course_id CHAR(100),\n" +
                        "                        _full INT,\n" +
                        "                        _delete INT,\n" +
                        "                        _submit INT,\n" +
                        "                        UNIQUE(student_id, course_id),\n" +
                        "                        PRIMARY KEY(id),\n" +
                        "                        FOREIGN KEY (student_id) REFERENCES student(id) ON DELETE CASCADE,\n" +
                        "                        FOREIGN KEY (course_id) REFERENCES course(id) ON DELETE CASCADE);", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }

    public Set<String> FindCoursesWaiters(String courseId) throws SQLException {
        OfferCommandInfo info = new OfferCommandInfo(new OfferInfo(null, courseId, null, null, null), OfferCommandType.FIND_WAITERS);
        return FindOffersBy(info);
    }

    public Set<String>  FindWaitedCourses() throws SQLException {
        OfferCommandInfo info = new OfferCommandInfo(new OfferInfo(null, null, null, null, null), OfferCommandType.FIND_WAITED);
        return FindOffersBy(info);
    }

    public Set<String>  FindOffersByStudentId(String stdId, Integer full, Integer submit) throws SQLException {
        OfferCommandInfo info = new OfferCommandInfo(new OfferInfo(stdId, null, full, submit, null), OfferCommandType.FIND_BY_STDID);
        return FindOffersBy(info);
    }

    public Set<String>  FindSameOffersByStudentId(String stdId, Integer full) throws SQLException { // find offers with the same type in capacity
        OfferCommandInfo info = new OfferCommandInfo(new OfferInfo(stdId, null, full, null, null), OfferCommandType.FIND_SAME_BY_STDID);
        return FindOffersBy(info);
    }

    public Set<String>  findRemovedOffers(String stdId, Integer full) throws SQLException {
        OfferCommandInfo info = new OfferCommandInfo(new OfferInfo(stdId, null, full, null, null), OfferCommandType.FIND_REMOVED);
        return FindOffersBy(info);
    }

    public String getFindOffersByStatement(OfferCommandType type) {
        switch (type) {
            case FIND_WAITERS:
                return  String.format("SELECT (offer.student_id) FROM %s offer WHERE offer.course_id = ? AND offer._full = 1 AND offer._submit = 1 AND offer._delete = 0;", TABLE_NAME);
            case FIND_WAITED:
                return  String.format("SELECT (offer.course_id) FROM %s offer WHERE offer._full = 1 AND offer._submit = 1 AND offer._delete = 0;", TABLE_NAME);
            case FIND_BY_STDID:
                return String.format("SELECT (offer.course_id) FROM %s offer WHERE offer.student_id = ? AND offer._full = ? AND offer._submit = ? AND offer._delete = 0;", TABLE_NAME);
            case FIND_SAME_BY_STDID:
                return String.format("SELECT (offer.course_id) FROM %s offer WHERE offer.student_id = ? AND offer._full = ? AND offer._delete = 0;", TABLE_NAME);
            case FIND_REMOVED:
                return String.format("SELECT (course_id) FROM %s offer WHERE offer.student_id = ? AND offer._submit = 1 AND offer._delete = 1 AND offer._full = ?;", TABLE_NAME);
        }
        return "";
    }

    private void fillFindOffersBy(PreparedStatement st, OfferCommandInfo info) throws SQLException {
        switch (info.GetCommandType()) {
            case FIND_WAITERS:
                st.setString(1, info.GetInfo().GetCourseId());
                break;
            case FIND_WAITED:
                break;
            case FIND_BY_STDID:
                st.setString(1, info.GetInfo().GetStudentId());
                st.setInt(2, info.GetInfo().GetFull());
                st.setInt(3, info.GetInfo().GetSubmitted());
                break;
            case FIND_SAME_BY_STDID:
            case FIND_REMOVED:
                st.setString(1, info.GetInfo().GetStudentId());
                st.setInt(2, info.GetInfo().GetFull());
                break;
        }
    }


    public String getFindOfferByStatement(OfferCommandInfo commandInfo) {
        if (commandInfo.GetInfo().GetSubmitted() == null) {
            return String.format("SELECT * FROM %s offer WHERE offer.student_id = ? AND offer.course_id = ? AND offer._delete = 0 AND offer._full = ?;", TABLE_NAME);
        }
        else {
            String deleteQuery = commandInfo.GetInfo().GetDeleted() < 0 ? "" : "AND offer._delete = ?";
            return String.format("SELECT (offer.course_id) FROM %s offer WHERE offer.student_id = ? AND offer.course_id = ? AND offer._full = ? AND offer._submit = ? %s;", TABLE_NAME, deleteQuery);
        }
    }

    public void fillFindOfferByStatement(PreparedStatement st, OfferCommandInfo commandInfo) throws SQLException {
        if (commandInfo.GetInfo().GetSubmitted() == null) {
            st.setString(1, commandInfo.GetInfo().GetStudentId());
            st.setString(2, commandInfo.GetInfo().GetCourseId());
            st.setInt(3, commandInfo.GetInfo().GetFull());
        }
        else {
            st.setString(1, commandInfo.GetInfo().GetStudentId());
            st.setString(2, commandInfo.GetInfo().GetCourseId());
            st.setInt(3, commandInfo.GetInfo().GetFull());
            st.setInt(4, commandInfo.GetInfo().GetSubmitted());
            if (commandInfo.GetInfo().GetDeleted() >= 0) {
                st.setInt(5, commandInfo.GetInfo().GetDeleted());
            }
        }
    }


    public String FindOfferBy(String stdId, String courseId, Integer full, Integer submit, Integer delete) throws SQLException {
        OfferCommandInfo info = new OfferCommandInfo(new OfferInfo(stdId, courseId, full, submit, delete), null);
        return findElement(info);
    }

    public String FindOfferBy(String stdId, String courseId, Integer full) throws SQLException {
        OfferCommandInfo info = new OfferCommandInfo(new OfferInfo(stdId, courseId, full, null, null), null);
        return findElement(info);
    }



/////////////////////////



    public void DeleteSelectedOffer(String stdId, String courseId, Integer full) throws SQLException {
        OfferCommandInfo commandInfo = new OfferCommandInfo(new OfferInfo(stdId, courseId, full), OfferCommandType.DELETE);
        transact(commandInfo);
    }

    public void SubmitOffers(String stdId) throws SQLException {
        OfferCommandInfo commandInfo = new OfferCommandInfo(new OfferInfo(stdId, null, null), OfferCommandType.SUBMIT);
        transact(commandInfo);
    }

    public void ResetOffers(String stdId) throws SQLException {
        OfferCommandInfo commandInfo = new OfferCommandInfo(new OfferInfo(stdId, null, null), OfferCommandType.RESET);
        transact(commandInfo);
    }

    public void ForceAddOffer(String stdId, String courseId) throws SQLException {
        OfferCommandInfo commandInfo = new OfferCommandInfo(new OfferInfo(stdId, courseId, null), OfferCommandType.FORCE_ADD);
        transact(commandInfo);
    }



    private String GetOfferCommandStatement(OfferCommandType type) {
        switch (type) {
            case RESET:
                return String.format("DELETE FROM %s WHERE id IN (SELECT * FROM (SELECT id FROM %s WHERE student_id = ? AND _submit = 0) AS temp);", TABLE_NAME, TABLE_NAME) +
                        String.format("UPDATE %s SET _delete = 0 WHERE id IN (SELECT * FROM (SELECT id FROM %s WHERE student_id = ? AND _submit = 1) AS temp);", TABLE_NAME, TABLE_NAME);
            case DELETE:
                return String.format("DELETE FROM %s WHERE id IN (SELECT * FROM (SELECT id FROM %s WHERE student_id = ? AND course_id = ? AND _submit = 0 AND _full = ?) AS temp);", TABLE_NAME, TABLE_NAME) +
                        String.format("UPDATE %s SET _delete = 1 WHERE id IN (SELECT * FROM (SELECT id FROM %s WHERE student_id = ? AND course_id = ? AND _submit = 1 AND _full = ?) AS temp);", TABLE_NAME, TABLE_NAME);
            case SUBMIT:
                return String.format("DELETE FROM %s WHERE id IN (SELECT * FROM (SELECT id FROM %s WHERE student_id = ? AND _delete = 1) AS temp);", TABLE_NAME, TABLE_NAME) +
                        String.format("UPDATE %s SET _submit = 1 WHERE id IN (SELECT * FROM (SELECT id FROM %s WHERE student_id = ? AND _submit = 0) AS temp);", TABLE_NAME, TABLE_NAME);
            case FORCE_ADD:
                return String.format("UPDATE %s SET _full = 0 WHERE id IN (SELECT * FROM (SELECT id FROM %s WHERE student_id = ? AND course_id = ?) AS temp);", TABLE_NAME, TABLE_NAME);
        }
        return "";
    }

    @Override
    protected String getTransactionStatement(Object obj) {
        OfferCommandInfo commandInfo = (OfferCommandInfo) obj;
        return GetOfferCommandStatement(commandInfo.GetCommandType());
    }

    @Override
    protected void fillTransactionValues(PreparedStatement st, Object obj) throws SQLException {
        OfferCommandInfo commandInfo = (OfferCommandInfo) obj;
        switch (commandInfo.GetCommandType()) {
            case RESET:
            case SUBMIT:
                st.setString(1, commandInfo.GetInfo().GetStudentId());
                st.setString(2, commandInfo.GetInfo().GetStudentId());
                break;
            case DELETE:
                st.setString(1, commandInfo.GetInfo().GetStudentId());
                st.setString(2, commandInfo.GetInfo().GetCourseId());
                st.setInt(3, commandInfo.GetInfo().GetFull());
                st.setString(4, commandInfo.GetInfo().GetStudentId());
                st.setString(5, commandInfo.GetInfo().GetCourseId());
                st.setInt(6, commandInfo.GetInfo().GetFull());
                break;
            case FORCE_ADD:
                st.setString(1, commandInfo.GetInfo().GetStudentId());
                st.setString(2, commandInfo.GetInfo().GetCourseId());
                break;
        }
    }

    @Override
    protected String getFindByIdStatement(Object obj) {
        return String.format("SELECT * FROM %s offering WHERE offering.id = ?;", TABLE_NAME);
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, Object obj) throws SQLException {
        String id = (String) obj;
        st.setString(1, id);
    }

    @Override
    protected String getInsertStatement() { //TODO edit
        return String.format("UPDATE %s SET _delete = 0 WHERE id IN (SELECT * FROM (SELECT id FROM %s WHERE student_id = ? AND course_id = ? AND _submit = 1) AS temp);", TABLE_NAME, TABLE_NAME) +
                String.format("INSERT INTO %s (student_id, course_id, _full, _delete, _submit) VALUES(?, ?, ?, ?, ?);", TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Offering offer) throws SQLException {
        st.setString(1, offer.GetStudentId());
        st.setString(2, offer.GetCourseId());
        st.setString(3, offer.GetStudentId());
        st.setString(4, offer.GetCourseId());
        st.setInt(5, offer.GetFullStatus());
        st.setInt(6, offer.GetDeleteStatus());
        st.setInt(7, offer.GetSubmitStatus());
    }

    @Override
    protected String getFindAllStatement(Object obj) {
        return String.format("SELECT * FROM %s;", TABLE_NAME);
    }

    @Override
    protected void fillFindAllValues(PreparedStatement st, Object obj) throws SQLException {

    }

    @Override
    protected Offering convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        Offering offer = new Offering(rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getInt(6));
        return offer;
    }

    @Override
    protected ArrayList<Offering> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<Offering> std = new ArrayList<>();
        while (rs.next()) {
            std.add(this.convertResultSetToDomainModel(rs));
        }
        return std;
    }



    private String convertResultSetToDMString(ResultSet rs) throws SQLException {
        return rs.getString(1);
    }

    private Set<String> convertResultSetToDomainModelSet(ResultSet rs) throws SQLException {
        Set<String> pres = new HashSet<String>();
        while (rs.next()) {
            String pr_course_id = rs.getString(1);
            pres.add(pr_course_id);
        }

        return pres;
    }

    public String findElement(OfferCommandInfo commandInfo) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(getFindOfferByStatement(commandInfo));
        try {
            fillFindOfferByStatement(st, commandInfo);
            ResultSet resultSet = st.executeQuery();
            if (!resultSet.next()) {
                st.close();
                con.close();
                return null;
            }
            String result = convertResultSetToDMString(resultSet);
            st.close();
            con.close();
            return result;
        } catch (Exception e) {
            st.close();
            con.close();
            System.out.println("error in Repository.find query.");
            e.printStackTrace();
            throw e;
        }
    }

    public Set<String> FindOffersBy(OfferCommandInfo commandInfo) throws SQLException {
        Connection con = ConnectionPool.getConnection();
//        System.out.println(stmt);
        PreparedStatement st = con.prepareStatement(getFindOffersByStatement(commandInfo.GetCommandType()));
        try {
            fillFindOffersBy(st, commandInfo);
            ResultSet resultSet = st.executeQuery();
            if (resultSet == null) {
                st.close();
                con.close();
                return new HashSet<String>();
            }
            Set<String> result = convertResultSetToDomainModelSet(resultSet);
            st.close();
            con.close();
            return result;
        } catch (Exception e) {
            st.close();
            con.close();
            System.out.println("error in Repository.findPrerequisites query.");
            e.printStackTrace();
            throw e;
        }
    }


}
