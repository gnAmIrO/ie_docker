package ie.ut.ac.ir.Bolbolestan.repository;

import ie.ut.ac.ir.Bolbolestan.repository.helper.UserInfo;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentRepo extends Repository<Student, String>{

    private static final String TABLE_NAME = "student";
    private static StudentRepo instance;

    public static StudentRepo getInstance(){
        if (instance == null) {
            try {
                instance = new StudentRepo();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in StudentRepository.create query.");
            }
        }
        return instance;
    }

    private StudentRepo() throws SQLException{
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s(id CHAR(50),\nname CHAR(100),\nlastname CHAR(100),\nfield CHAR(80),\nfaculty CHAR(100),\nstatus CHAR(100),\nlevel CHAR(100),\nimg CHAR(250),\nbirthDate CHAR(100),\nemail CHAR(100) UNIQUE,\npassword CHAR(200), PRIMARY KEY(id));", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }

    public Student FindStudent(String id, boolean isEmail) throws SQLException {
        UserInfo info = new UserInfo(id, null, isEmail);
        return findById(info);
    }

    @Override
    protected String getFindByIdStatement(Object obj) { // TODO:
        UserInfo info = (UserInfo) obj;
        return String.format("SELECT * FROM %s student WHERE student.%s = ?;", TABLE_NAME, info.isIdEmail() ? "email" : "id");
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, Object obj) throws SQLException { // TODO:
        UserInfo info = (UserInfo) obj;
        st.setString(1, info.GetId());
    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s (id, name, lastname, field, faculty, status, level, img, birthDate, email, password) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Student std) throws SQLException {
        st.setString(1, std.GetInfo().GetId());
        st.setString(2, std.GetInfo().GetFirstName());
        st.setString(3, std.GetInfo().GetLastName());
        st.setString(4, std.GetInfo().GetField());
        st.setString(5, std.GetInfo().GetFaculty());
        st.setString(6, std.GetInfo().GetStatus());
        st.setString(7, std.GetInfo().GetLevel());
        st.setString(8, std.GetInfo().GetImg());
        st.setString(9, std.GetInfo().GetBirthDate());
        st.setString(10, std.GetInfo().GetEmail());
        st.setString(11, std.GetInfo().GetPassword());
    }

    @Override
    protected String getFindAllStatement(Object obj) {
        return String.format("SELECT * FROM %s;", TABLE_NAME);
    }

    @Override
    protected void fillFindAllValues(PreparedStatement st, Object obj) throws SQLException {

    }

    @Override
    protected Student convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        Student std = new Student(rs.getString(2),
                rs.getString(3),
                rs.getString(1),
                "",
                rs.getString(9),
                rs.getString(4),
                rs.getString(5),
                rs.getString(7),
                rs.getString(6),
                rs.getString(8),
                rs.getString(10),
                rs.getString(11));
        return std;
    }

    @Override
    protected ArrayList<Student> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<Student> std = new ArrayList<>();
        while (rs.next()) {
            std.add(this.convertResultSetToDomainModel(rs));
        }
        return std;
    }

    public void UpdatePassword(String stdId, String password) throws SQLException { // TODO:
        UserInfo info = new UserInfo(stdId, password, false);
        transact(info);
    }

    @Override
    protected void fillTransactionValues(PreparedStatement st, Object obj) throws SQLException {
        UserInfo info = (UserInfo) obj;
        st.setString(1, info.GetPassword());
        st.setString(2, info.GetId());
    }

    @Override
    protected String getTransactionStatement(Object obj) {
        return String.format(
            "UPDATE %s SET password = ? WHERE id = ?;", TABLE_NAME);
    }


}
