package ie.ut.ac.ir.Bolbolestan.services.model.Logic;

import ie.ut.ac.ir.Bolbolestan.model.Offering;
import ie.ut.ac.ir.Bolbolestan.repository.OfferingRepo;
import ie.ut.ac.ir.Bolbolestan.services.model.Exception.CourseAlreadyAddedException;
import ie.ut.ac.ir.Bolbolestan.services.model.Exception.ErrorHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.Exception.PrerequisitesException;
import ie.ut.ac.ir.Bolbolestan.services.model.Exception.TimeOverlapException;
import ie.ut.ac.ir.Bolbolestan.services.model.Helper.Constants;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.Course;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.CourseRepoInterface;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.Student;

import java.sql.SQLException;
import java.util.*;

public class WeeklySchedule {
    private OfferingRepo offerRepository = OfferingRepo.getInstance();
    private Map<String, String> weeklySchedule;
    private Set<String> finalizedCourses;
    private Student student;


    public WeeklySchedule(Student student) {
        this.weeklySchedule   = new HashMap<>();
        this.finalizedCourses = new HashSet<>();
        this.student = student;
    }

    public List<String> GetWeeklySchedule() {
        try {
            return new ArrayList<>(offerRepository.FindOffersByStudentId(student.GetInfo().GetId(), 0, 0));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("GetWeeklySchedule --> Unable to Get Weekly courses");
            return null;
        }
    }

    public Set<String> GetFinalizedCourses() {
        try {
            return offerRepository.FindOffersByStudentId(student.GetInfo().GetId(), 0, 1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("GetFinalizedCourses --> Unable to Get finalized courses");
            return null;
        }
    }

    public void DeleteNonFinalizedCourses(){
        weeklySchedule.entrySet().removeIf(entry -> entry.getValue().equals(Constants.NON_FINALIZED));
    }

    public boolean DoesCourseExistInWeeklySchedule(String courseId) {
        String courseClassCode = courseId.substring(0, courseId.length() - 2);
        try {
            if (offerRepository.FindOfferBy(student.GetInfo().GetId(), courseClassCode + "01", 0) != null) return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("DoesCourseExistInWeeklySchedule --> Error in finding course in db");
        }
//        for (String id: weeklySchedule.keySet()) {
//            if (id.substring(0, id.length() - 2).equals(courseClassCode))
//                return true;
//        }
        return false;
    }

    public boolean IsCourseFinalized(String courseId) {
        try {
            String tmp = offerRepository.FindOfferBy(student.GetInfo().GetId(), courseId, 0, 1, -1);
            return tmp != null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("IsCourseFinalized --> Error in checking course is finalized");
            return false;
        }
    }

    public void AddCourseToWeeklySchedule(String courseId) throws CourseAlreadyAddedException, PrerequisitesException, TimeOverlapException {
        Course course = CourseRepoInterface.GetCourse(courseId);
        System.out.println(courseId + " ");
        if (DoesCourseExistInWeeklySchedule(courseId))
            throw new CourseAlreadyAddedException(Constants.OFFERING_ALREADY_EXISTS);

        if (!student.HasPassedAllPrerequisites(course.GetInfo().GetPrerequisites()))
            throw new PrerequisitesException(Constants.NOT_PASS_PREREQUISITES);

        if (student.HasOverlap(courseId)) {
            throw new TimeOverlapException(Constants.OVERLAP_IN_SCHEDULE);
        }
        Offering newOffer = new Offering(student.GetInfo().GetId(), courseId, 0, 0, 0);
        try {
            offerRepository.insert(newOffer);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("AddCourseToWeeklySchedule --> Error in inserting new Offer");
        }
    }

    public void ForceAddCourseToWeeklySchedule(String courseId)  {
//        Course course = CourseRepoInterface.GetCourse(courseId);
        System.out.println(courseId + " force ");

        try {
            offerRepository.ForceAddOffer(student.GetInfo().GetId(), courseId);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("ForceAddCourseToWeeklySchedule --> Error in Changing Waiting course status");
        }
    }

    public void RemoveCourseFromWeeklySchedule(String courseId) throws ErrorHandler {
        if (!DoesCourseExistInWeeklySchedule(courseId))
            throw new ErrorHandler(Constants.OFFERING_NOT_FOUND);

        try {
            offerRepository.DeleteSelectedOffer(student.GetInfo().GetId(), courseId, 0);
        } catch (SQLException throwables) {
            System.out.println("RemoveCourseFromWeeklySchedule --> Unable to delete course, Error in query");
        }
    }

    public Integer GetNumOfSelectedUnits() { //TODO: aval submit mikonee ba'd check mikoneee ke dars ha capacity ro rad kardan??
        Integer numOfSelectedUnits = 0;
//        for (String id : finalizedCourses) {
//            Integer numOfUnitsOfSelectedCourse = CourseRepoInterface.GetCourse(id).GetInfo().GetNumOfUnits();
//            numOfSelectedUnits += numOfUnitsOfSelectedCourse;
//
//        }
        try {
            Set<String> weeklySchedule = offerRepository.FindOffersByStudentId(student.GetInfo().GetId(), 0, 1);
            for (String id : weeklySchedule) {
                Integer numOfUnitsOfSelectedCourse = CourseRepoInterface.GetCourse(id).GetInfo().GetNumOfUnits();
                numOfSelectedUnits += numOfUnitsOfSelectedCourse;

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("GetNumOfSelectedUnits ->> Error in Getting offer num of unit");
            numOfSelectedUnits = 0;
        }
        System.out.println(numOfSelectedUnits);
        return numOfSelectedUnits;
    }

    public boolean IsNumOfSelectedUnitsOverCapacity(Integer numOfSelectedUnits) {
        return numOfSelectedUnits > Constants.MAX_NUM_OF_UNITS;
    }

    public boolean IsNumOfSelectedUnitsUnderCapacity(Integer numOfSelectedUnits) {
        return numOfSelectedUnits < Constants.MIN_NUM_OF_UNITS;
    }


    public boolean IsNumOfSelectedUnitsValid(Integer numOfSelectedUnits) {
        return !IsNumOfSelectedUnitsUnderCapacity(numOfSelectedUnits) && !IsNumOfSelectedUnitsOverCapacity(numOfSelectedUnits);
    }



    public void ResetWeeklySchedule() {
//        weeklySchedule.clear();
//        for (String courseId: finalizedCourses) {
//            weeklySchedule.put(courseId, Constants.FINALIZED);
//        }
//        System.out.println(student.GetInfo().GetId());
        try {
            offerRepository.ResetOffers(student.GetInfo().GetId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("ResetWeeklySchedule --> Error in submitting courses");
        }
    }

    public void HandleFinalizeWeeklySchedule()  {
        FinalizeWeeklySchedule();
    }

    public boolean ShouldFinalizeWeeklySchedule(List<Course> fullCourses) {
        if (!IsNumOfSelectedUnitsValid(GetNumOfSelectedUnits()))
            return false;

//        if (!overlappingCourses.isEmpty())
//            return false;

        return fullCourses.isEmpty();
    }


    private void FinalizeWeeklySchedule() {
//        finalizedCourses.clear();
//        Set<String> courseIds = weeklySchedule.keySet();
////        finalizedCourses.addAll(courseIds);
//        for (String courseId: courseIds) {
//            System.out.println(courseId);
//            weeklySchedule.replace(courseId, Constants.FINALIZED);
//        }

        try {
            offerRepository.SubmitOffers(student.GetInfo().GetId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("FinalizeWeeklySchedule --> Error in submitting courses");
        }

    }

    public boolean DoesCourseExistInFinalizedSchedule(String courseId) {
        try {
            return offerRepository.FindOfferBy(student.GetInfo().GetId(), courseId, 0, 1, 0) != null;
        } catch (SQLException throwables) {
            System.out.println("DoesCourseExistInFinalizedSchedule --> Error in searching for finding finalizedCourse");
            return false;
        }
    }

    public Set<String> GetRemovedCourses() {
        Set<String> removedCourses = null;
        try {
            removedCourses = offerRepository.findRemovedOffers(student.GetInfo().GetId(), 0);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return removedCourses;
    }
}
