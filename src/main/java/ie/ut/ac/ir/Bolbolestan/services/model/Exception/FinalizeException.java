package ie.ut.ac.ir.Bolbolestan.services.model.Exception;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.Course;
import org.javatuples.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FinalizeException extends Throwable {
    private List<Course> fullCourses;
    private boolean isOverUnit;
    private boolean isUnderUnit;


    private List<String> GenerateErrors() {
        List<String> errors = new ArrayList<>();

        if (isUnderUnit) {
            errors.add("MinimumUnitsError");
        }
        if (isOverUnit) {
            errors.add("MaximumUnitsError");
        }
//        for (Pair<String, String> coursePair: overlappingCourses.keySet()) {
//            errors.add(String.format("%sTimeCollisionError %s %s", overlappingCourses.get(coursePair), coursePair.getValue0(), coursePair.getValue1()));
//        }
        for (Course course: fullCourses) {
            errors.add(String.format("CapacityError %s", course.GetInfo().GetId()));
        }

        return errors;
    }

    public FinalizeException(List<Course> fullCourses, boolean isOverUnit, boolean isUnderUnit) {

        this.fullCourses        = fullCourses;
        this.isOverUnit         = isOverUnit;
        this.isUnderUnit        = isUnderUnit;

    }

    public List<String> Message() {
        return GenerateErrors();
    }
}


