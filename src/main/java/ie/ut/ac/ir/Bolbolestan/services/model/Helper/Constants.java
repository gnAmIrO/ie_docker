package ie.ut.ac.ir.Bolbolestan.services.model.Helper;

import java.util.ArrayList;
import java.util.List;

public class Constants {
    public static final String STUDENT_ID = "id";
    public static final String NAME = "name";
    public static final String LAST_NAME = "secondName";
    public static final String BIRTH_DATE = "birthDate";
    public static final String ENTERED_AT = "enteredAt";

    public static final String ADD_TO_WEEKLY_SCHEDULE = "addToWeeklySchedule";
    public static final String REMOVE_FROM_WEEKLY_SCHEDULE = "removeFromWeeklySchedule";


    public static final String CODE = "code";
    public static final String CAPACITY = "capacity";
    public static final String PREREQUISITES = "prerequisites";
    public static final String STATUS = "status";
    public static final String DEFAULT_FILTER = "All";



    public static final String FINALIZED = "finalized";
    public static final String NON_FINALIZED = "non-finalized";

    public static final String OK = "OK";
    public static final String BAD = "BAD";

    public static final String BIRTH_DATE_FORMAT = "yyyy/MM/dd";


    public static final String OFFERING_NOT_FOUND = "OfferingNotFound";
    public static final String OFFERING_ALREADY_EXISTS = "OfferingAlreadyExists";
    public static final String OFFERING_ALREADY_FINALIZED = "OfferingAlreadyFinalized";
    public static final String STUDENT_ALREADY_EXISTS = "StudentAlreadyExists";
    public static final String STUDENT_NOT_FOUND = "StudentNotFound";
    public static final String NOT_PASS_PREREQUISITES = "PrerequisitesNotPassed";
    public static final String OVERLAP_IN_SCHEDULE = "OverlapInScheduleCourses";


    public static final String WEEKLY_SCHEDULE = "weeklySchedule";
    public static final int    MIN_NUM_OF_UNITS = -1;
    public static final int    MAX_NUM_OF_UNITS = 20;
    public static final String COURSE_NOT_PASSED = "CourseNotPassed";
    public static final String GRADE = "grade";

    public static final String URL_STD_ID = "studentId";
    public static final String URL_COURSE_ID = "courseId";
    public static final String URL_COURSE_CODE = "courseCode";

    public static final String Course_Form_STD_ID = "std_id";

    public static final String[] DAYS = {"saturday", "sunday", "monday", "tuesday", "wednesday"};
    public static final String[] TIMES = {"7:30-9:00", "9:00-10:30", "10:30-12:00", "14:00-15:30", "16:00-17:30"};
    public static final String COURSE_NOT_ENROLLED = "CourseNotEnrolled";
    public static final String PRIMARY = "primary";
    public static final String ID = "id";
    public static final String SECOND_NAME = "secondName";
    public static final String SEARCH_ACTION = "search";
    public static final String ADD_TO_WAIT = "wait";
    public static final String REMOVE_FROM_WAIT = "no_wait";
    public static final String TERM = "term";
    public static final String FIELD = "field";
    public static final String FACULTY = "faculty";
    public static final String LEVEL = "level";
    public static final String IMG = "img";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final int COURSEDB_ID_INDEX = 1;
    public static final int COURSEDB_NAME_INDEX = 2;
    public static final int COURSEDB_CLASSID_INDEX = 3;
    public static final int COURSEDB_GROUPID_INDEX = 4;
    public static final int COURSEDB_INSTRUCTORNAME_INDEX = 5;
    public static final int COURSEDB_NUMOFUNITS_INDEX = 6;
    public static final int COURSEDB_CLASSTIMEDAYS_INDEX = 7;
    public static final int COURSEDB_CLASSTIME_IN_INDEX = 8;
    public static final int COURSEDB_CLASSTIME_OUT_INDEX = 9;
    public static final int COURSEDB_EXAMTIME_INDEX = 10;
    public static final int COURSEDB_CAPACITY_INDEX = 11;
    public static final int COURSEDB_NUMOFENROLLED_INDEX = 12;
    public static final int COURSEDB_TYPE_INDEX = 13;
    public static final int COURSEDB_PREREQUISITES_INDEX = 14;

    public static String GradesUrl = "http://138.197.181.131:5200/api/grades";
    public static String CoursesUrl = "http://138.197.181.131:5200/api/courses";
    public static String StudentsUrl = "http://138.197.181.131:5200/api/students";
    public static Integer WaitStatus = 0;
    public static Integer SubmitStatus = 1;
    public static Integer WaitingStatus = 2;
    public static Integer SubmittingStatus = 3;

    public static String HashSecretKey = "bolbolestan";

    public static String emailSendApi = "http://138.197.181.131:5200/api/send_mail";

    public static String resetPasswordLink = "http://localhost:3000/resetPassword?";
}
