package ie.ut.ac.ir.Bolbolestan.model;

public class Prerequisite {
    private String courseId;
    private String prCourseId;


    public Prerequisite(String courseId, String prCourseId){
        this.courseId       = courseId;
        this.prCourseId      = prCourseId + "01";
    }

    public String GetCourseId() { return courseId; }
    public String GetPrerequisite(){ return prCourseId; }
}
