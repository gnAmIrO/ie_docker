package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time;
import org.json.simple.JSONObject;

import java.time.Instant;

public class Interval {
    private Instant begin;
    private Instant end;

    public Interval(long begin, long end) {
        this.begin = Instant.ofEpochMilli(begin);
        this.end   = Instant.ofEpochMilli(end);
    }

    public static boolean HasOverlap(Interval t1, Interval t2) {
        boolean hasOverlapClosed        = !(t1.end.isBefore(t2.begin) || t1.begin.isAfter(t2.end)); // assumes that intervals are closed, it returns true for 13-14:30 and 14:30-16;
        boolean areEndAndBeginningsSame = (t1.end.equals(t2.begin) || t2.end.equals(t1.begin));     // for example 13-14:30 and 14:30-16 have same beginning and end;

        if (hasOverlapClosed) {
            return !areEndAndBeginningsSame; // for example 13-14:30 and 14:30-16 have same beginning and end, but don't overlap;
        }
        return false;
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        obj.put("begin", begin.toString());
        obj.put("end", end.toString());
        return obj;
    }
    public JSONObject toJson(String begin, String end) {
        JSONObject obj = new JSONObject();
        obj.put("begin", begin);
        obj.put("end", end);
        return obj;
    }
}
