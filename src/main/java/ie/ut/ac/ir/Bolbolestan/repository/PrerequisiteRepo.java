package ie.ut.ac.ir.Bolbolestan.repository;

import ie.ut.ac.ir.Bolbolestan.model.Bolbol;
import ie.ut.ac.ir.Bolbolestan.model.Prerequisite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PrerequisiteRepo extends Repository<Prerequisite, String> {
    private static final String TABLE_NAME = "prerequisite";
    private static PrerequisiteRepo instance;

    public static PrerequisiteRepo getInstance() {
        if (instance == null) {
            try {
                instance = new PrerequisiteRepo();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in PrerequisiteRepo.create query.");
            }
        }
        return instance;
    }

    private PrerequisiteRepo() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s(id MEDIUMINT NOT NULL AUTO_INCREMENT,\n" +
                                "                        course_id CHAR(100),\n" +
                                "                        pr_course_id CHAR(100),\n" +
                                "                        UNIQUE(course_id, pr_course_id),\n" +
                                "                        PRIMARY KEY(id),\n" +
                                "                        FOREIGN KEY (course_id) REFERENCES course(id) ON DELETE CASCADE,\n" +
                                "                        FOREIGN KEY (pr_course_id) REFERENCES course(id) ON DELETE CASCADE);", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }

    @Override
    protected String getFindByIdStatement(Object obj) {
        return String.format("SELECT (pr.pr_course_id) FROM %s pr WHERE pr.course_id = ?;", TABLE_NAME);
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, Object obj) throws SQLException {
        String id = (String) obj;
        st.setString(1, id);
    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s(course_id, pr_course_id) VALUES(?,?);", TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Prerequisite data) throws SQLException {
        st.setString(1, data.GetCourseId());
        st.setString(2, data.GetPrerequisite());
    }

    @Override
    protected String getFindAllStatement(Object obj) {
        return String.format("SELECT * FROM %s;", TABLE_NAME);
    }

    @Override
    protected void fillFindAllValues(PreparedStatement st, Object obj) throws SQLException {

    }

    @Override
    protected Prerequisite convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        Prerequisite pr = new Prerequisite(rs.getString(2), rs.getString(3));
        return pr;
    }

    @Override
    protected ArrayList<Prerequisite> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<Prerequisite> pres = new ArrayList<>();
        while (rs.next()) {
            pres.add(this.convertResultSetToDomainModel(rs));
        }
        return pres;
    }

    @Override
    protected void fillTransactionValues(PreparedStatement preparedStatement, Object obj) throws SQLException {

    }

    @Override
    protected String getTransactionStatement(Object obj) {
        return null;
    }


    private Set<String> convertResultSetToDomainModelSet(ResultSet rs) throws SQLException {
        Set<String> pres = new HashSet<String>();
        while (rs.next()) {
            String pr_course_id = rs.getString(1);
            pres.add(pr_course_id.substring(0, pr_course_id.length()-2));
        }

        return pres;
    }


    public Set<String> findPrerequisites(String id) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(getFindByIdStatement(id));
        try {
            fillFindByIdValues(st, id);
            ResultSet resultSet = st.executeQuery();
            if (resultSet == null) {
                st.close();
                con.close();
                return new HashSet<String>();
            }
            Set<String> result = convertResultSetToDomainModelSet(resultSet);
            st.close();
            con.close();
            return result;
        } catch (Exception e) {
            st.close();
            con.close();
            System.out.println("error in Repository.findPrerequisites query.");
            e.printStackTrace();
            throw e;
        }
    }
}
