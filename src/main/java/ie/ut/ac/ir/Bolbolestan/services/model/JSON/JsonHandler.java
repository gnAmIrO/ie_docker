package ie.ut.ac.ir.Bolbolestan.services.model.JSON;

import org.json.JSONObject;
import org.json.JSONArray;


public class JsonHandler {
    public static JSONObject DeserializeJsonObject(String stringToParse) { return new JSONObject(stringToParse); }

    public static JSONArray DeserializeJsonArray(String stringToParse) {
        return new JSONArray(stringToParse);
    }

    public static String SerializeJsonData(JSONObject JsonData) {
        return JsonData.toString();
    }

    public static String SerializeJsonData(JSONArray JsonData) {
        return JsonData.toString();
    }

}


