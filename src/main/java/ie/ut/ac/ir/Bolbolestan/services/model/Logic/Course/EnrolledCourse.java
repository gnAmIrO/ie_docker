package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course;

import org.json.JSONObject;

public class EnrolledCourse {
    private final String id;

    private final Float grade;
    private final int numOfUnits;
    private final int term;
    private final String name;
    public EnrolledCourse(String id, Float grade, int numOfUnits, int term, String name) {
        this.id = id;
        this.grade = grade;
        this.numOfUnits = numOfUnits;
        this.term = term;
        this.name = name;
    }


    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        obj.put("id", id);
        obj.put("grade", grade);
        obj.put("numOfUnits", numOfUnits);
        obj.put("term", term);
        obj.put("name", name);
        return obj;
    }

    public String GetId() {
        return id;
    }

    public float GetGrade() {
        return grade;
    }

    public int GetNumOfUnits() {
        return numOfUnits;
    }

    public int GetTerm() {
        return term;
    }
}
