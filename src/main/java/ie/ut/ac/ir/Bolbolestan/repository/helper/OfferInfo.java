package ie.ut.ac.ir.Bolbolestan.repository.helper;

public class OfferInfo {
    private String studentId;
    private String courseId;
    private Integer full;
    private Integer submitted;
    private Integer deleted;


    public OfferInfo(String studentId, String courseId, Integer full) {
        this(studentId, courseId, full, null, null);
    }

    public OfferInfo(String studentId, String courseId, Integer full, Integer submitted) {
        this(studentId, courseId, full, submitted, null);
    }

    public OfferInfo(String studentId, String courseId, Integer full, Integer submitted, Integer deleted) {
        this.studentId = studentId;
        this.courseId = courseId;
        this.full = full;
        this.submitted = submitted;
        this.deleted = deleted;
    }

    public Integer GetFull() {
        return full;
    }

    public String GetCourseId() {
        return courseId;
    }

    public String GetStudentId() {
        return studentId;
    }

    public Integer GetSubmitted() {
        return submitted;
    }

    public Integer GetDeleted() {
        return deleted;
    }
}
