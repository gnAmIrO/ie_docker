package ie.ut.ac.ir.Bolbolestan.controllers;


import ie.ut.ac.ir.Bolbolestan.controllers.Utilities.DelayManager;
import ie.ut.ac.ir.Bolbolestan.controllers.models.LoginModel;
import ie.ut.ac.ir.Bolbolestan.controllers.Utilities.ResponseWrapper;
import ie.ut.ac.ir.Bolbolestan.controllers.models.SignupModel;
import ie.ut.ac.ir.Bolbolestan.repository.StudentRepo;
import ie.ut.ac.ir.Bolbolestan.services.model.API.APIHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.Helper.Constants;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Student.Student;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static ie.ut.ac.ir.Bolbolestan.services.model.Helper.Constants.HashSecretKey;


@RestController
public class LoginController {

    private StudentRepo studentRepo = StudentRepo.getInstance();
    @Autowired
    private APIHandler apiHandler;

    private String getJWTToken(String email, long exp_time) {
        String secretKey = HashSecretKey;
        final long JWT_TOKEN_VALIDITY = exp_time;
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId(email)
                .setSubject(email)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuer("bolbolestan")
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return token;
    }

    private JSONObject EmailContent(String email, String jwtToken){
        JSONObject content = new JSONObject();
        content.put("email", email);
        content.put("url", Constants.resetPasswordLink + jwtToken);

        return content;
    }

    @RequestMapping(value = "/auth/login/{test}", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getLoginData(@RequestParam String username, @PathVariable String test){

        ResponseWrapper responseWrapper = new ResponseWrapper("hello this is new Id with Json",  HttpStatus.OK);


        return new ResponseEntity(responseWrapper.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/auth/login", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> postLoginData(@RequestBody LoginModel loginModel){
        String email = loginModel.getEmail();
        String password = loginModel.getPassword();

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        try {
            Student student = studentRepo.FindStudent(email, true);
            if(student == null){
                ResponseWrapper responseWrapper = new ResponseWrapper("نام کاربربری یا رمز عبور اشتباه است", HttpStatus.NOT_FOUND);
                DelayManager.Delay(3);
                return new ResponseEntity(responseWrapper.toString(), HttpStatus.NOT_FOUND);
            }else{
                String StudentHashedPassword = student.GetInfo().GetPassword();

                if(bCryptPasswordEncoder.matches(password, StudentHashedPassword)){
                    ResponseWrapper responseWrapper = new ResponseWrapper(getJWTToken(email, 24*60*60*1000), HttpStatus.OK);
                    DelayManager.Delay(3);
                    return new ResponseEntity(responseWrapper.toString(), HttpStatus.OK);
                }else{
                    ResponseWrapper responseWrapper = new ResponseWrapper("نام کاربری یا رمز عبور اشتباه است", HttpStatus.NOT_FOUND);
                    DelayManager.Delay(3);
                    return new ResponseEntity(responseWrapper.toString(), HttpStatus.NOT_FOUND);
                }
            }
        } catch (Exception throwables) {
            ResponseWrapper responseWrapper = new ResponseWrapper("مشکلی در سرور یا پایگاه داده اتفاق افتاده است", HttpStatus.SERVICE_UNAVAILABLE);
            DelayManager.Delay(3);
            return new ResponseEntity(responseWrapper.toString(), HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @RequestMapping(value="/auth/login/forget", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> postResetData(@RequestBody LoginModel loginModel){
        String email = loginModel.getEmail();
        try {
            Student student = studentRepo.FindStudent(email, true);
            if(student == null){
                ResponseWrapper responseWrapper = new ResponseWrapper("نام کاربری یا رمز عبور اشتباه است", HttpStatus.NOT_FOUND);
                DelayManager.Delay(3);
                return new ResponseEntity(responseWrapper.toString(), HttpStatus.NOT_FOUND);
            }else{
                apiHandler.SendEmail(Constants.emailSendApi, EmailContent(email, getJWTToken(email, 10*60*1000)));
                ResponseWrapper responseWrapper = new ResponseWrapper("ok", HttpStatus.OK);
                DelayManager.Delay(3);
                return new ResponseEntity(responseWrapper.toString(), HttpStatus.OK);
            }
        } catch (Exception throwables) {
            ResponseWrapper responseWrapper = new ResponseWrapper("مشکلی در سرور یا پایگاه داده اتفاق افتاده است", HttpStatus.SERVICE_UNAVAILABLE);
            DelayManager.Delay(3);
            return new ResponseEntity(responseWrapper.toString(), HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @RequestMapping(value="/auth/signup", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> postSignupData(@RequestBody SignupModel signupModel){
        String email = signupModel.getEmail();
        System.out.println(email);
        try {
            Student student = new Student(signupModel.getFirstname(), signupModel.getLastname(), signupModel.getStdId(), "", signupModel.getBirthDate(), signupModel.getMajor(), signupModel.getFaculty(), signupModel.getGrade(), "مشغول به تحصیل", "http://138.197.181.131:5200/img/default.jpg", email, signupModel.getPassword());
            student.GetInfo().HashPassword();
            studentRepo.insert(student);
            ResponseWrapper responseWrapper = new ResponseWrapper(getJWTToken(email, 24*60*60*1000), HttpStatus.OK);
            DelayManager.Delay(3);
            return new ResponseEntity(responseWrapper.toString(), HttpStatus.OK);

        } catch (Exception throwables) {
            ResponseWrapper responseWrapper = new ResponseWrapper("مشکلی در سرور یا پایگاه داده اتفاق افتاده است", HttpStatus.SERVICE_UNAVAILABLE);
            DelayManager.Delay(3);
            return new ResponseEntity(responseWrapper.toString(), HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @RequestMapping(value = "/login/reset", method= RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> GetProfileData(@RequestBody LoginModel loginModel){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String stdId =  SecurityContextHolder.getContext().getAuthentication().getName();
        System.out.println(stdId);
        String password = loginModel.getPassword();
        try {
            studentRepo.UpdatePassword(stdId, bCryptPasswordEncoder.encode(password));
            return new ResponseEntity("reset password", HttpStatus.OK);
        } catch (SQLException throwables) {
//            throwables.printStackTrace();
            return new ResponseEntity("مشکلی در پایگاه داده اتفاق افتاده است", HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

}
