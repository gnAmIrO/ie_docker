package ie.ut.ac.ir.Bolbolestan.services.model.JSON;
import org.json.JSONObject;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class JsonExtractor {
    public Map<String, Object> Extract(JSONObject objToBeExtracted, Set<String> keys) {
        Map<String, Object> values = new HashMap<>();
        for (String key: keys) {
           values.put(key, objToBeExtracted.get(key));
        }
        return values;
    }
}
