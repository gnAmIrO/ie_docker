package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time;

import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course.CourseRepository;

public class WaitResolveJob implements Runnable {

    @Override
    public void run() {
        // Do your hourly job here.
        System.out.println("Job triggered by scheduler");
        CourseRepository.GetInstance().EnrollWaiters();
    }
}