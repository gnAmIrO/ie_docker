package ie.ut.ac.ir.Bolbolestan.services.model.API;
import ie.ut.ac.ir.Bolbolestan.services.model.HTTP.RequestCommand;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class APIHandler {
    public String RequestToAPI(String url) {
        RequestCommand request = new RequestCommand();
        request.ExecuteGetRequest(url);
        String responseBody = request.GetResponse();
        return responseBody;
    }

    public String SendEmail(String url, JSONObject requestBody){
        RequestCommand request = new RequestCommand();
        request.ExecutePostRequest(url, requestBody);
        String responseBody = request.GetResponse();
        return responseBody;
    }
}
