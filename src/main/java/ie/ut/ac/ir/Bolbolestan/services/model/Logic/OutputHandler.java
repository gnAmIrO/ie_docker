package ie.ut.ac.ir.Bolbolestan.services.model.Logic;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class OutputHandler {
    public static String SuccessOutput(Map obj){
        Map output = new LinkedHashMap();

        output.put("success", "true");
        output.put("data", obj);
        return JSONObject.toJSONString(output);
    }

    public static String SuccessOutput(ArrayList obj){
        Map output = new LinkedHashMap();
        output.put("success", "true");
        output.put("data", obj);
        return JSONObject.toJSONString(output);
    }

    public static String SuccessOutput(String obj){
        Map output = new LinkedHashMap();
        output.put("success", "true");
        output.put("data", obj);
        return JSONObject.toJSONString(output);
    }

    public static String ErrorOutput(String Message){
        Map output = new LinkedHashMap();
        output.put("success", "false");
        output.put("data", Message);
        return JSONObject.toJSONString(output);
    }

    public static String ErrorOutput(List<String> Message){
        Map output = new LinkedHashMap();
        output.put("success", "false");
        output.put("data", Message);
        return JSONObject.toJSONString(output);
    }
}
