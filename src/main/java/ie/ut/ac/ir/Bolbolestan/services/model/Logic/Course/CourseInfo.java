package ie.ut.ac.ir.Bolbolestan.services.model.Logic.Course;
import java.util.*;

import ie.ut.ac.ir.Bolbolestan.services.model.JSON.JsonHandler;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time.ClassTime;
import ie.ut.ac.ir.Bolbolestan.services.model.Logic.Time.ExamTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class CourseInfo {
    private String  name;
    private String id;
    private String classId;
    private String groupId;
    private String  instructorName;
    private Integer numOfUnits;
    private ClassTime classTime;
    private ExamTime examTime;
    private Integer  capacity;
    private String   type;
    private Set<String> prerequisites;

    public CourseInfo(String name, String classId, String groupId, String instructorName, Integer numOfUnits, ClassTime classTime, ExamTime examTime, Integer capacity, String type) {
        this.name = name;
        this.classId = classId;
        this.groupId = groupId;
        this.id = classId + groupId;
        this.instructorName = instructorName;
        this.numOfUnits = numOfUnits;
        this.classTime = classTime;
        this.examTime = examTime;
        this.capacity = capacity;
        this.type     = type;
        this.prerequisites = new HashSet<String>();
    }

    public void SetPrerequisites(Set<String> prerequisites){
        this.prerequisites = prerequisites;
    }
    public Set<String> GetPrerequisites() {
        return prerequisites;
    }

    public Integer GetCapacity() {
        return capacity;
    }

    public Integer GetNumOfUnits() {
        return numOfUnits;
    }

    public String GetType() { return type; }

    public String GetId() { return id; }

    public String GetGroupId() { return groupId; }

    public String GetClassId() { return classId; }

    public String GetName() { return name; }

    public String GetInstructorName(){ return instructorName; }

    public ClassTime GetClassTime() { return classTime; }

    public ExamTime  GetExamTime()  { return examTime; }

    @Override
    public boolean equals(Object obj) {

        if (obj == this)
            return true;

        if (!(obj instanceof CourseInfo))
            return false;

        CourseInfo courseInfo = (CourseInfo) obj;

        return id.equals(courseInfo.GetId());
    }

    @Override
    public String toString() {

        return JSONObject.toJSONString(toMap());
    }


    public Map<String, Object> toMap(){
        Map<String, Object> obj = new LinkedHashMap<String, Object>();

        obj.put("id", classId);
        obj.put("group-id", groupId);
        obj.put("name", name);
        obj.put("type", type);
        obj.put("Instructor", instructorName);
        obj.put("units", numOfUnits);
        obj.put("classTime", JsonHandler.DeserializeJsonObject(classTime.toString()));
        obj.put("examTime", JsonHandler.DeserializeJsonObject(examTime.toString()));
        obj.put("capacity", capacity);
        obj.put("prerequisites", JsonHandler.DeserializeJsonArray(JSONArray.toJSONString(new ArrayList<>(prerequisites))));

        return obj;
    }

    public JSONObject toJson(){
        JSONObject obj = new JSONObject();

        obj.put("id", classId);
        obj.put("group-id", groupId);
        obj.put("name", name);
        obj.put("Instructor", instructorName);
        obj.put("units", numOfUnits);
        obj.put("classTime", classTime.toJson());
        obj.put("type", type);

        return obj;
    }


}
