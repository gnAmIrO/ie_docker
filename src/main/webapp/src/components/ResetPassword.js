import '../css/signup-login.css';
import LoadingIndicator from './LoadingIndicator';
import React from 'react';
import axios from 'axios'
import { Link, useLocation } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { usePromiseTracker } from "react-promise-tracker";
import { trackPromise } from 'react-promise-tracker';
import ErrorModal from './ErrorModal.js'
import SuccessModal from './SuccessModal.js'

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function ResetPassword() {
    let  resetToken  = useQuery();
    const [errorMessage, setErrorMessage] = React.useState('');
    const [successMessage, setSuccessMessage] = React.useState('');
    const {promiseInProgress} = usePromiseTracker()
    
    function handleResetPassword(event){
        event.preventDefault();
        let password = event.target.elements.password.value
        let confirmPassword = event.target.elements.confirmPassword.value
        let resetInfo = {
            'email': '',
            'password': password 
        }
        if(password != confirmPassword){
            setErrorMessage('تکرار رمز عبور صحیح نیست');
            setSuccessMessage('')
        }else{   
            trackPromise(
                axios.post('http://localhost:8080/login/reset', resetInfo,  {headers: {
                      'Authorization': `Bearer ${resetToken}`
                    }})
                .then(res => {
                    window.location.href = '/'
                    console.log("response: -->82 ");
                    console.log(res);
                    })
                .catch(err => {
                console.log(err.response.status)
                    setSuccessMessage('')
                    setErrorMessage('ایمیل یافت نشد');
                })
            ); 
        }
    }

    return (
        <div className="container signup-login-container">
            <div className="row center">
                <div className="col-sm-2">
                </div>
                <div className="col-sm-8 reset-form">
                    <form onSubmit={handleResetPassword}>
                        <h3>بازنشانی رمز عبور</h3>
                        <div className="reset-form-logo">
                        <img src="../images/logo.png" alt="logo pic"/>
                        </div>
                        <div className="form-group">
                            <input type="password" className="form-control" name="password" placeholder="رمز عبور"/>
                        </div>
                        
                        <div className="form-group">
                            <input type="password" className="form-control" name="confirmPassword" placeholder="تکرار رمز عبور"/>
                        </div>

                        <div className="form-group">
                        <input type="submit" className="btnSubmit" value="تعویض رمز ورود" readOnly/>
                        </div>
                    </form>
                </div>
                <div className="col-sm-2">
                </div>
                <LoadingIndicator promiseInProgress={promiseInProgress} type={"ThreeDots"} indicator_style={"Loading-Indicator-login-signup"}/>
            </div>   
            <ErrorModal errorMessage = {errorMessage} />
            <SuccessModal successMessage = {successMessage} />     
        </div>
    )
}



export default ResetPassword;