import React from "react";
import Loader from "react-loader-spinner";

const LoadingIndicator = props => {
    return (
        props.promiseInProgress &&
        <div className={props.indicator_style}>
            <Loader type={props.type} color="#2BAD60" height="100" width="100" />
        </div>
    );  
}


export default LoadingIndicator;