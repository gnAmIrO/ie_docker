import React from "react";
import '../css/normalize.css'
import '../css/header.css'
import '../css/font/flaticon.css'
import { Link } from 'react-router-dom';


function Header_Modal(){

    function exitHandler(){
        if(localStorage.getItem('token'))
            localStorage.removeItem('token')
        
       window.location.href="/login";
    }

    return(
        <div class="modal" tabindex="-1" role="dialog" id="exitModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-body">آیا می‌خواهید از حساب کاربری خود خارج شوید؟</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn color-gray border-gray" id="cancel" data-dismiss="modal">انصراف</button>
                        <button type="button" class="btn color-white bg-green" id="exit" onClick={exitHandler}>خروج</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

function Header(props){

    return(
        <>
        <header>
            <Link to="/"><img class="brand" src="images/logo.png" alt="logo" /></Link>
            <nav>
                <ul class="nav-links">
                    <li><Link to={props.links[0]}>{props.addresses[0]}</Link></li>
                    <li><Link to={props.links[1]}>{props.addresses[1]}</Link></li>
                </ul>
            </nav>
            <button id="logout" type="button" class="logout-button" data-toggle="modal" data-target="#exitModal"><span>خروج</span> <span><i class="flaticon-log-out"></i></span></button>
        </header>
        <Header_Modal />
        </>
    );
}





export default Header;


