import '../css/footer.css';
import '../css/signup-login.css';
import React from 'react';
import axios from 'axios'
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { usePromiseTracker } from "react-promise-tracker";
import { trackPromise } from 'react-promise-tracker';
import LoadingIndicator from './LoadingIndicator';
import ErrorModal from './ErrorModal.js'
import SuccessModal from './SuccessModal.js'


function setCurrentUserWithExpiry(token){
  localStorage.setItem('token', token)
}

function validateEmail (emailAdress){
  let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (emailAdress.match(regexEmail)) {
    return true; 
  } else {
    return false; 
  }
}

function validateDate(date){
  var date_regex =  /^[0-9]{4}\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/;
  if ((date_regex.test(date))) {
      return true
  }
  console.log('no-32')
  return false
}





function ForgetPassword(props){
  // const [resetInfo, setResetInfo] = React.useState({});

  function handleResetSubmit(event){
    event.preventDefault();
    props.setSuccessMessage('')
    props.setErrorMessage('');

    let resetInfo = {
      'email': event.target.elements.email.value
    };

    if(!validateEmail(resetInfo.email)){
      props.setErrorMessage('فرمت ایمیل درست نمی‌باشد');
      props.setSuccessMessage('')
    }else{
      
      trackPromise(
        axios.post('http://87.247.185.122:30379/auth/login/forget', resetInfo)
        .then(res => {
            props.setSuccessMessage('لینک تغییر رمز عبور به ایمیل ارسال شد')
            props.setErrorMessage('');
              })
        .catch(err => {
          props.setSuccessMessage('')
          props.setErrorMessage('ایمیل یافت نشد');
          
        })
      ); 
    }
  }

  return(
    <div className="modal fade" id="forgetPassword" tabindex="-1" role="dialog" aria-labelledby="forgetPasswordTitle" aria-hidden="true">
      <div className="modal-dialog modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="forgetPasswordTitle">لطفا ایمیل ثبت شده در سامانه خود را وارد کنید</h5>
            <span aria-hidden="true" data-dismiss="modal" aria-label="Close">&times;</span>
          </div>
          <form onSubmit={handleResetSubmit}>
          <div className="modal-body">
            <div className="form-group">
              <input type="text" className="form-control" placeholder="ایمیل" name="email"/>
            </div>    
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-dismiss="modal">خروج</button>
            <button type="submit" className="btn btn-success">ثبت نام</button>
          </div>
          </form>
        </div>
      </div>
    </div>
  );
}

function LoginForm(){
  const [errorMessage, setErrorMessage] = React.useState('');
  const [successMessage, setSuccessMessage] = React.useState('');
  function handleLoginSubmit(event){
    event.preventDefault();
    setErrorMessage('');
    let loginInfo = {
      'email': event.target.elements.email.value,
      'password': event.target.elements.password.value
    }
    if(!validateEmail(loginInfo.email)){
      setErrorMessage('فرمت ایمیل درست نمی‌باشد');
    }else{
      
      trackPromise(
        axios.post('http://87.247.185.122:30379/auth/login', loginInfo)
        .then(res => {
            window.location.href="/";
            setCurrentUserWithExpiry(res.data.content);
              })
        .catch(err => {
          localStorage.removeItem('token');
          setErrorMessage(err.response.data.content);
          
        })
      ); 
    }
  }

  return(
    <>
    <ForgetPassword setErrorMessage={setErrorMessage} setSuccessMessage={setSuccessMessage}/>{console.log('-->' + successMessage)}
    <form onSubmit={handleLoginSubmit}>
      <h3>ورود</h3>
      <div className="form-group">
          <input type="text" className="form-control" placeholder="ایمیل" name="email"/>
      </div>
      <div className="form-group">
          <input type="password" className="form-control" placeholder="رمز عبور" name="password" />
      </div>
      <div className="form-group">
        <input type="submit" className="btnSubmit" value="ورود" readOnly/>
      </div>
      <div className="form-group">
        <Link to="/login" className="btnForgetPwd" data-toggle="modal" data-target="#forgetPassword">پسوردتو فراموش کردی؟</Link>
      </div>
    </form>
    <ErrorModal errorMessage = {errorMessage} />
    <SuccessModal successMessage = {successMessage} />
    </>
  );
}

function SignUpInfoModal(props){
  

  function handleSignupSubmit(event){ 
    console.log('hey')
    props.setErrorMessage('');
    event.preventDefault();
    let signupInfo = {
      'stdId': props.signupInfo.username,
      'email': props.signupInfo.email,
      'password': props.signupInfo.password,
      'firstname': event.target.elements.firstname.value,
      'lastname': event.target.elements.lastname.value,
      'birthDate': event.target.elements.birthDate.value,
      'major': event.target.elements.major.value, 
      'faculty': event.target.elements.faculty.value,
      'grade': event.target.elements.grade.value,
    };

    if(!validateEmail(signupInfo.email)){
      props.setErrorMessage('فرمت ایمیل داده شده غلط می باشد');
    }else if(!validateDate(signupInfo.birthDate)){
      props.setErrorMessage('باشد yyyy/mm/dd تاریخ تولد به صورت');
    }else{
      
      trackPromise(
        axios.post('http://87.247.185.122:30379/auth/signup', signupInfo)
        .then(res => {
            window.location.href="/";
            console.log("response: -->82 ");
            console.log(res);
            setCurrentUserWithExpiry(res.data.content);
            
              })
        .catch(err => {
          localStorage.removeItem('token');
          console.log(err.response.status)
          props.setErrorMessage('ثبت نام موفقیت آمیز نبود');
        })
      );
    }   
  }

  return(
    <>
    <div className="modal fade" id="signupInfo" tabindex="-1" role="dialog" aria-labelledby="signupInfoTitle" aria-hidden="true">
      <div className="modal-dialog modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="signupInfoTitle">لطفا برای ثبت نام اطلاعات زیر را کامل کنید</h5>
            <span aria-hidden="true" data-dismiss="modal" aria-label="Close">&times;</span>
          </div>
          <form onSubmit={handleSignupSubmit}>
          <div className="modal-body">
            <div className="form-group">
              <input type="text" className="form-control" placeholder="شماره دانشجویی" name="username" value ={props.signupInfo.username} readonly/>
            </div>
            
            <div className="form-group">
                <input type="text" className="form-control" placeholder="ایمیل" name="email" value ={props.signupInfo.email} readonly/>
            </div>
            <div className="form-group">
                <input type="text" className="form-control" placeholder="نام" name="firstname" />
            </div>
            <div className="form-group">
                <input type="text" className="form-control" placeholder="نام خانوادگی" name="lastname" />
            </div>
            
            <div className="form-group">
                <input type="text" className="form-control" placeholder="تاریخ تولد" name="birthDate" />
            </div>
            <div className="form-group">
                <input type="text" className="form-control" placeholder="رشته" name="major" />
            </div>
            <div className="form-group">
                <input type="text" className="form-control" placeholder="دانشکده" name="faculty" />
            </div>
            <div className="form-group">
                <input type="text" className="form-control" placeholder="مقطع" name="grade" />
            </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-dismiss="modal">خروج</button>
            <button type="submit" className="btn btn-success">ثبت نام</button>
          </div>
          </form>
        </div>
      </div>
    </div>
    
    </>
  )
}
function SignupForm(){
  let [signupInfo, setSignupInfo] = React.useState({});
  const [errorMessage, setErrorMessage] = React.useState('');

  function handleSignupSubmit(event){
    setErrorMessage('')
    event.preventDefault();
    console.log('submitted ' + event.target.elements.username.value);
    setSignupInfo({
      'username': event.target.elements.username.value,
      'email': event.target.elements.email.value,
      'password': event.target.elements.password.value
    })
  }

  return(
    <>
      <SignUpInfoModal signupInfo={signupInfo} setErrorMessage={setErrorMessage}/>
      <form onSubmit={handleSignupSubmit}>
        <h3>ثبت نام</h3>
        <div className="signup-login-logo">
          <img src="../images/logo.png" alt="logo pic"/>
        </div>
        <div className="form-group">
            <input type="text" className="form-control" placeholder="شماره دانشجویی" name="username" />
        </div>
        
        <div className="form-group">
            <input type="text" className="form-control" placeholder="ایمیل" name="email" />
        </div>
        <div className="form-group">
            <input type="password" className="form-control" placeholder="رمز عبور" name="password" />
        </div>

        <div className="form-group">
          <input type="submit" className="btnSubmit" value="ثبت نام" readOnly data-toggle="modal" data-target="#signupInfo"/>
        </div>
      </form>
      <ErrorModal errorMessage = {errorMessage} />
    </>
  );
}

function Login() {
  const {promiseInProgress} = usePromiseTracker()
  return (
    <div className="container signup-login-container">
      <div className="row center">
        
        <div className="col-sm-6 login-form">
          <LoginForm />
        </div>
        <div className="col-sm-6 signup-form">
          <SignupForm />
        </div>
        <LoadingIndicator promiseInProgress={promiseInProgress} type={"ThreeDots"} indicator_style={"Loading-Indicator-login-signup"}/>
      </div>        
    </div>
  )
}

export default Login;
