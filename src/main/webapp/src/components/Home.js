import React, {useState, useEffect} from "react";
import axios from 'axios'
import EnglishNum2Arabic from '../js/ArabicFilter.js'
import '../css/styles.css'
import '../css/home.css'




function Info(props) {
  return (
    <div className="content-flex content-vertically-center pt-5 profile-size">
      <div className="user-picture mb-5"><img src={props.image} alt="avatar"/></div>
      <p className="mt-1 mb-3"><span className="color-light-blue">نام:</span> {props.name} </p>
      <p className="mb-3"><span className="color-light-blue">شماره دانشجویی:</span> {EnglishNum2Arabic(props.id)} </p>
      <p className="mb-3"><span className="color-light-blue">تاریخ تولد:</span> {EnglishNum2Arabic(props.date)} </p>
      <p className="mb-3"><span className="color-light-blue">معدل کل:</span> {EnglishNum2Arabic(parseFloat(props.gpu).toFixed(2))} </p>
      <p className="mb-3"><span className="color-light-blue">واحد گذرانده:</span> {EnglishNum2Arabic(props.unitsNo)} </p>
      <p className="mb-3"><span className="color-light-blue">دانشکده:</span> {props.faculty} </p>
      <p className="mb-3"><span className="color-light-blue">رشته:</span> {props.field} </p>
      <p className="mb-3"><span className="color-light-blue">مقطع:</span> {props.degree} </p>
      <p className="border-blue color-blue radius-3 p-2 pr-4 pl-4"><span> {props.status} </span></p>
    </div>

  );
}

function SemesterReport(props) {
  const semesterNo = EnglishNum2Arabic(props.semesterNo)
  
  return (
    <div className="semester border-blue radius-10 scrollable pt-5 pl-2 pr-2">
      <div><p className="absolute-top-left border-blue radius-10 p-1">کارنامه - ترم {semesterNo}</p></div>
      {props.semester.courses.map((course, index) => { 
        return ( 
          <Course index={index + 1} grade={course.grade} id={course.id} 
          numOfUnits={course.numOfUnits} name={course.name}/>); })
      }
      <div className="border-blue p-2 pr-4 pl-4 average color-blue radius-3 ">معدل: {EnglishNum2Arabic(parseFloat(props.semester.gpu).toFixed(2))}</div>
    </div>
  );
}

function Course(props) {
  const color = props.grade >= 10 ? 'green' : 'red' 
  return (
    <div className="content-flex units">
      <div className="color-blue border-light-blue" > <h6> {EnglishNum2Arabic(props.index)}</h6> </div>
      <div className="border-light-blue"> <p> {EnglishNum2Arabic(props.id)}</p> </div>
      <div className="border-light-blue"> <p> {props.name} </p> </div>
      <div className="border-light-blue"> <p> {EnglishNum2Arabic(props.numOfUnits) + 'واحد'} </p> </div>
      <div className={"border-light-blue color-" + color}> <p className={"background-" + color}>{props.grade > 10 ? 'قبول' :'مردود'}</p> </div>
      <div className={"border-light-blue color-" + color}> <p>{EnglishNum2Arabic(props.grade)}</p> </div>
    </div>

  );
}


function Home() {
  var token = localStorage.getItem('token')
  

  const [fullName, setFullName] = useState('تست تستی')
  const [id, setId] = useState('0')
  const [date, setDate] = useState('۱۳۷۶/۶/۶')
  const [gpu, setGPU] = useState('۱۶.۹۲')
  const [unitsNo, setUnitsNo] = useState('۴۲۰')
  const [faculty, setFaculty] = useState('پردیس دانشکده هنر')
  const [field, setField] = useState('مهندسی برق')
  const [degree, setDegree] = useState('کاردانی')
  const [status, setStatus] = useState('مشغول به تحصیل')
  const [image, setImage] = useState('https://www.pngfind.com/pngs/m/538-5381574_view-samegoogleiqdbsaucenao-gooby-goofy-from-mickey-mouse-meme.png') 
  const [semesters, setSemesters] = useState({})

  useEffect(() => {
    
    if(!id){
      return;
    }
    axios.get('http://87.247.185.122:30379/profile', {headers: {
                      'Authorization': `Bearer ${token}`
                    }}).then(
      res => {
        const info = res.data.info
        setFullName(info.name)
        setId(info.id)
        setDate(info.birthDate)
        setGPU (info.GPU)
        setUnitsNo(info.numPassedUnits)
        setFaculty(info.faculty)
        setField(info.field)
        setDegree(info.level)
        setStatus(info.status)
        setImage(info.img)
        setSemesters(res.data.semesters)
      }
    ).catch(err => {
          localStorage.removeItem('token');
        })
    
  }, [id]);

  return (
    <div className="home">
      <div className="_content">
        <div id="carouselExampleIndicators" className="carousel slide l2r" data-ride="carousel">

          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          </ol>

          <div className="carousel-inner">
            <div className="carousel-item active slide1"></div>
            <div className="carousel-item slide2"></div>
          </div>

          <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
          </a>
          
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-3 col-lg-3">
              <Info name={fullName} id={id} date={date} gpu={gpu} unitsNo={unitsNo} faculty={faculty} field={field} degree={degree} status={status} image={image}/>
            </div>
            <div className="col-sm-9 col-lg-9 border-right-blue">
              <div className="_content-flex content-vertically-center scrollable profile-size">
                {Object.keys(semesters).map(semesterNum => <SemesterReport semester={semesters[semesterNum]} semesterNo={semesterNum}/>)}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
