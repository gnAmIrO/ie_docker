import React from "react";
import '../css/courses.css'
import '../css/font/flaticon.css'
import axios from 'axios'
import EnglishNum2Arabic from '../js/ArabicFilter.js'
import GetCurrentStudent from '../js/GetCurrentStudent.js'
import { ToastContainer, toast } from 'react-toastify';
import SearchBox from './SearchBox.js'
import OfferingFilter from './OfferingFilter.js'
import convertTime2HH_MM from '../js/TimeConvertor.js'
import { usePromiseTracker } from "react-promise-tracker";
import { trackPromise } from 'react-promise-tracker';
import LoadingIndicator from './LoadingIndicator';

const filter_type = {
  0 : 'All',
  1 : 'Takhasosi',
  2 : 'Asli',
  3 : 'Paaye',
  4 : 'Umumi'
}

const course_type = {
  'Takhasosi' : ['takhasosi', 'تخصصی'],
  'Asli' : ['asli', 'اصلی'],
  'Paaye' : ['paye', 'پایه'],
  'Umumi' : ['omomi', 'عمومی']
}

const days = {
  'monday' : 'دوشنبه',
  'tuesday' : 'سه شنبه', 
  'wednesday' : 'چهارشنبه', 
  'thursday' : 'پنجشنبه', 
  'friday' : 'جمعه', 
  'saturday' : 'شنبه', 
  'sunday' : 'یکشنبه'
}


const ConvertToValidFormat = (course, courseStatus) =>{
  const json = {
      code: course.id,
      group_id: course["group-id"],
      courseName: course.name,
      instructor: course.Instructor,
      unit: course.units,
      status: courseStatus
    }
    
    return json;
}

const ErrorHandler = Message =>{
  toast.error(Message, {
    position: "top-center",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
}

const SuccessHandler = Message =>{
  toast.success(Message, {
    position: "top-center",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
}

function DeleteFromStateArray(state, setState, target) {
  let temp = []
  temp = state.filter((value, index, arr) => {
    return value.code != target
  });
  setState(temp)
}

function SelectedOffering(props){

  function deleteSelectedOffering(){
    DeleteFromStateArray(props.arr[0], props.arr[1], props.course.code)
    props.deleteOffering[1]([props.course.code, props.course.group_id, props.course.status])
  }

  return (
    <tr>
      <td  onClick={deleteSelectedOffering}>
        <div>
          <i className="flaticon-trash-bin color-red delete-icon"></i>
        </div>
      </td>
      <td>
        {props.course.status == 1 ? 
        <div class="waiting-course">
            <p>در انتظار</p>
        </div> : 
        props.course.status == 0 ? 
        <div class="non-finlaized-course">
            <p>ثبت نهایی نشده</p>
        </div> :
        props.course.status == 2 ?
        <div class="finalized-course">
            <p>ثبت شده</p>
        </div> : 
        <div></div>
        }
      </td>
      <td>{EnglishNum2Arabic(props.course.code)}-{EnglishNum2Arabic(props.course.group_id)}</td>
      <td>{props.course.courseName}</td>
      <td>{props.course.instructor}</td>
      <td className="num-units">{EnglishNum2Arabic(props.course.unit)}</td>
    </tr>
  );
}

function SubmitNavbar(props){

  function resetHandler(){
    // Reset()
    props.renderValue[1](['reset'])
  }

  function submitHandler(){
    props.submitValue[1](props.submitValue[0] + 1)
  }

  

  return(
    <div class="table-caption border-light-blue radius-bottom">    
        <div class="num-of-selected-units color-dark-blue">
            <span>تعداد واحد های ثبت شده: {EnglishNum2Arabic(props.totalFinalizedUnitCount)}</span>
        </div>
        <form>
        <div class="pair">
          <button type="button" id="reset" class="btn refresh-button" onClick={resetHandler}>
            <span class="refresh-button-icon"><i class="flaticon-refresh-arrow color-white center"></i></span>
          </button> 

          <button type="button" id="submit" class="btn finalize-button color-white" onClick={submitHandler}>
              <span class="color-white">ثبت نهایی</span>
          </button>       
        </div>   
        </form>
    </div>
  );
}


function SelectedOfferings(props){
  
  let renderCourse = React.useState(['default'])
  let onSubmitValue = React.useState(0)
  const [weeklySchedule, setWeeklySchedule] = React.useState(0)
  const [totalFinalizedUnitCount, setTotalFinalizedUnitCount] = React.useState(0) 

  React.useEffect(() => { // reset || delete
    const token = GetCurrentStudent()
    if (token == -1) return;

    if(renderCourse[0][0] === 'reset' || renderCourse[0][0] === 'default'){
      RequestResetCourses(weeklySchedule, setWeeklySchedule, token)
    }
    else if(!renderCourse[0][0].includes('reset') ){ // delete
      const courseUrl = renderCourse[0][0] + renderCourse[0][1]
      DeleteSelectedOffering(courseUrl, token)
    }
    
  },[renderCourse[0]])

  React.useEffect(() => { // submit
    let token = GetCurrentStudent()
    if (token == -1) return;

    if(onSubmitValue[0] != 0){
      SubmitSelectedOfferings(token)
      // renderCourse[1]('reset' + weeklySchedule)
      setWeeklySchedule(weeklySchedule + 1);
      props.submitCourses[1](props.submitCourses[0] + 1)
    }
    
  }, [onSubmitValue[0]])

  React.useEffect( () => {
    let token = GetCurrentStudent()
    if(!token){
      return;
    }
    GetFinalizedCourses(props.finalizedCourses[1], props.waitedCourses[1], props.selectedCourses[1], token, setTotalFinalizedUnitCount)
  }, [weeklySchedule])
 
  return(
    <div class="container content">
      <div class=" border-light-blue box-wrapper selected-courses radius-top">
        <div class="absolute-top-left round-borders">
          <p>دروس انتخاب شده</p>
        </div>
        <div class="no-overflow selected-courses-table">
            <div class="table-responsive">
              <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th scope="col"></th>
                          <th scope="col"><p>وضعیت</p></th>
                          <th scope="col"><p>کد</p></th>
                          <th scope="col"><p>نام درس</p></th>
                          <th scope="col"><p>استاد</p></th>
                          <th scope="col"><p>واحد</p></th>
                      </tr>
                  </thead>
                  <tbody>
                    {props.finalizedCourses[0].map(course => {console.log(course); return (<SelectedOffering course={course} deleteOffering = {renderCourse} courselists = {0} arr={props.finalizedCourses}/>)} )}
                    {props.waitedCourses[0].map(course => {console.log(course); return (<SelectedOffering course={course} deleteOffering = {renderCourse} courselists = {1} arr={props.waitedCourses}/>) })}
                    {props.selectedCourses[0].map(course => {console.log(course); return (<SelectedOffering course={course} deleteOffering = {renderCourse} courselists = {2} arr={props.selectedCourses}/>)})}
                     
                  </tbody>
                </table>
          </div>
        </div>
    </div>
    <SubmitNavbar submitValue = {onSubmitValue} renderValue={renderCourse} totalFinalizedUnitCount={totalFinalizedUnitCount}/>
    
    </div>
  );
}






function Offering(props){
  const courseInfo = props.course.info
  
  function handleAddCourse(){
    const json = ConvertToValidFormat(courseInfo, props.course.enrolled >= props.course.info.capacity)
    if(!props.selectedCourses.find(element => element.code + element.group_id == json.code + json.group_id)){
      props.chosenCourse[1](json)
    }

    
    
  }
   
  // TODO: regex 4 exam time
  return(
     <tr onClick={handleAddCourse}>
        <td>
          {props.course.enrolled == courseInfo.capacity ? <i className="flaticon-clock-circular-outline icon full" /> : <i className="flaticon-add icon not-full" />}
        </td>
        <td>
          {EnglishNum2Arabic(courseInfo.id)}-{EnglishNum2Arabic(courseInfo["group-id"])}
        </td>
        <td className= "num-units">{EnglishNum2Arabic(props.course.enrolled)}/{EnglishNum2Arabic(courseInfo.capacity)}</td>
        <td><span className={"unitType " + course_type[courseInfo.type][0]}>{course_type[courseInfo.type][1]}</span></td>
        <td>{courseInfo.name}</td>
        <td>{courseInfo.Instructor}</td>
        <td className="num-units">{EnglishNum2Arabic(courseInfo.units)}</td>
        <td> 
          <div className="callout left">
            <span>{EnglishNum2Arabic(courseInfo.classTime.time)} <br/> {days[courseInfo.classTime.days[0]]} - {days[courseInfo.classTime.days[1]]} </span>
            <span>
              <span className="h6">پیش‌نیازی‌ها</span>
              {EnglishNum2Arabic(courseInfo.prerequisites.toString())} 
              <span className="h6">امتحان</span>
              {EnglishNum2Arabic(convertTime2HH_MM(courseInfo.examTime.end.split('T')[1]))} - {EnglishNum2Arabic(convertTime2HH_MM(courseInfo.examTime.start.split('T')[1]))} {courseInfo.examTime.start.split('T')[0]} 
            </span>
          </div> 
        </td>
    </tr>
  );
}

function Offerings(props){
  const headers = ['کد', 'وضعیت', 'نوع', 'نام درس', 'استاد', 'واحد', 'توضیحات']
  
  const [offerings, setOfferings] = React.useState({});

  React.useEffect(() => {
    let token = GetCurrentStudent()
    if(token == -1) return;
    console.log(token);
    const dataBody = {
      'id' : token,
      'search' : props.search[0],
      'filter' : filter_type[props.filter[0]]
    }
    GetOfferings(setOfferings, dataBody, token);
    
  }, [props.filter[0], props.search[0], props.submitCourses[0]])

  return(
    
    <div className="no-overflow courses-table">
      <div className="table-responsive">
        <table className="table table-bordered">
          <thead>
            <tr>
              <th scope="col"></th>
              {headers.map(_header => <th scope="col"><p>{_header}</p></th>)}
            </tr>
          </thead>
          <tbody>
            {Object.keys(offerings).map(courseId => <Offering chosenCourse={props.chosenCourse} course={offerings[courseId]} courseId={courseId} selectedCourses={props.selectedCourses}/>)}
          </tbody>
        </table>
      </div>
    </div>
  );
}

function SubmitSelectedOfferings(token) {
  axios
  .get('http://87.247.185.122:30379/courses/submit', {headers: {
    'Authorization': `Bearer ${token}`
  }})
  .then(res => {
      console.log("submit " + res.data)
      if(res.data.ok){
        SuccessHandler(res.data.ok)
      }
      if(res.data.error){
        ErrorHandler(res.data.error)
      }

    })
  .catch(err => {
    console.log(err)
  });
}

function DeleteSelectedOffering(courseUrl, token) {
  trackPromise(
  axios
  .delete('http://87.247.185.122:30379/courses/' + courseUrl, {headers: {
    'Authorization': `Bearer ${token}`
  }})
  .then(res => {

    if(res.data.ok){
      console.log(res.data.ok)
    }
    if(res.data.error){
      console.log(res.data.error)
    }
  })
  .catch(err => {
    console.log(err)
  }));
}

function RequestResetCourses(weeklySchedule, setWeeklySchedule, token) {
  trackPromise(
  axios
  .delete('http://87.247.185.122:30379/courses', {headers: {
    'Authorization': `Bearer ${token}`
  }})
  .then(res => {
      console.log("reset " + res.data)
      setWeeklySchedule(weeklySchedule + 1);
    })
  .catch(err => {
    console.log(err)
  }));
}

function GetFinalizedCourses(finalizedCoursesSetter, waitingCoursesSetter, selectedCoursesSetter, token, setTotalFinalizedUnitCount) {
  let finalizedCourses = []
  let waitingCourses = []
  
  axios
  .get("http://87.247.185.122:30379/courses/weeklySchedule", {headers: {
    'Authorization': `Bearer ${token}`
  }})
  .then(res => {
    // console.log(res.data)
    res.data.finalizedCourses.map((course) => {finalizedCourses.push(ConvertToValidFormat(course, 2)); console.log(course.id)})
    res.data.finalizedWaitingCourses.map((course) => {waitingCourses.push(ConvertToValidFormat(course, 1));})
    setTotalFinalizedUnitCount(res.data.totalSubmittedCourses)
    finalizedCoursesSetter(finalizedCourses)
    waitingCoursesSetter(waitingCourses)
    selectedCoursesSetter([])
    // console.log("375-->")
    
  })
  .catch(err => {
    console.log(err)
  });
}

function GetOfferings(setCourses, dataBody, token) {
  trackPromise(
  axios
  .post('http://87.247.185.122:30379/courses', dataBody, {headers: {
    'Authorization': `Bearer ${token}`
  }})
  .then(res => {
    setCourses(res.data);  
  })
  .catch(err => {
    localStorage.removeItem('token');
  }));
}

function PostSelectedCourse(selectedCourse, setSelectedCourse, nonFinalizedCourses, setNonFinalizedCourses, dataBody, token) {
  const courseUrl = selectedCourse.code + selectedCourse.group_id
  let selectedCourses = [...nonFinalizedCourses]
  trackPromise(
  axios
  .post('http://87.247.185.122:30379/courses/' + courseUrl, dataBody, {headers: {
    'Authorization': `Bearer ${token}`
  }})
  .then(res => {
    console.log("add ", res.data)
    if(res.data.ok){
      if(Object.keys(selectedCourse).length > 0){
        selectedCourses.push(selectedCourse)
      }
    }
    if(res.data.error){
      ErrorHandler(res.data.error)
    }
    setSelectedCourse({})
    setNonFinalizedCourses(selectedCourses)
  })
  .catch(err => {
    console.log(err.data)
  }));

}


function Courses() {
  const [finalizedWaitingCourses, setFinalizedWaitingCourses] = React.useState([])
  const [finalizedCourses, setFinalizedCourses] = React.useState([])
  const [nonFinalizedCourses, setNonFinalizedCourses] = React.useState([])
  
  const [searchFilter, setSearchFilter] = React.useState("")
  const [offeringFilter, setOfferingFilter] = React.useState(0)
  const [selectedCourse, setSelectedCourse] = React.useState({})
  const [submitCourses, setSubmitCourses] = React.useState(false)


  React.useEffect(() => { // Add effect
    if(Object.keys(selectedCourse).length !== 0){
      const token = GetCurrentStudent()

      const dataBody = {
        'id' : token,
        'search': '',
        'filter': '',
        'status' : selectedCourse.status ? '1' : '0'
      }
      
      PostSelectedCourse(selectedCourse, setSelectedCourse, nonFinalizedCourses, setNonFinalizedCourses, dataBody, token)
    }
  }, [selectedCourse]);

  const {promiseInProgress} = usePromiseTracker()
  return (
    <div className="container content">
      <SelectedOfferings chosenCourse = {[selectedCourse, setSelectedCourse]} submitCourses={[submitCourses, setSubmitCourses]} waitedCourses={[finalizedWaitingCourses, setFinalizedWaitingCourses]} finalizedCourses={[finalizedCourses, setFinalizedCourses]} selectedCourses={[nonFinalizedCourses, setNonFinalizedCourses]}/>
      <LoadingIndicator promiseInProgress={promiseInProgress} type={"Bars"} indicator_style={"Loading-Indicator-login-signup"}/>
      <SearchBox search ={[searchFilter, setSearchFilter]}/>
      <div className=" border-light-blue box-wrapper courses radius-top">
        <div className="absolute-top-left round-borders">
          <p>دروس ارائه شده</p>
        </div>

        <OfferingFilter filter={[offeringFilter, setOfferingFilter]} />
        <Offerings search ={[searchFilter, setSearchFilter]} filter = {[offeringFilter, setOfferingFilter]} chosenCourse={[selectedCourse, setSelectedCourse]} selectedCourses={[nonFinalizedCourses, setNonFinalizedCourses]} submitCourses={[submitCourses, setSubmitCourses]}/>
        </div>

        <div className={'mt-5'}>
          <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl
          pauseOnFocusLoss
          draggable
          pauseOnHover
          />
        </div>
    </div>
  );
}

export default Courses;
