import React from 'react'

function OfferingFilter(props){

    return(
      <div className="content-flex filter">
        <div className={props.filter[0] == 0 ? "selected" : "non-selected"} onClick={() => {props.filter[1](0)}}>همه</div>
        <div className={props.filter[0] == 1 ? "selected" : "non-selected"} onClick={() => {props.filter[1](1)}}>اختصاصی</div>
        <div className={props.filter[0] == 2 ? "selected" : "non-selected"} onClick={() => {props.filter[1](2)}}>اصلی</div>
        <div className={props.filter[0] == 3 ? "selected" : "non-selected"} onClick={() => {props.filter[1](3)}}>پایه</div>
        <div className={props.filter[0] == 4 ? "selected" : "non-selected"} onClick={() => {props.filter[1](4)}}>عمومی</div>
      </div>
    );
}
export default OfferingFilter;