import React from 'react'

function SearchBox(props){

    function submitHandler(event){
      event.preventDefault();
      props.search[1](event.target.elements.search.value)
    }
  
  
    return(
      <div className="row searchBox">
          <div className="col-sm-1 col-md-2"></div>
          <div className="col-12 col-sm-10 col-md-8">
            <form onSubmit={submitHandler}>
              <div className="input-group search-box">
                <input type="text" className="form-control" placeholder="نام درس" aria-label="course" name="search" />
                <button className="btn btn-outline-secondary" type="submit"><span className="pair"><span>جستجو</span> <span><i className="flaticon-loupe"></i></span></span></button>
              </div>
            </form>
          </div>
          <div className="col-sm-1 col-md-2"></div>
      </div>
    );
  }

export default SearchBox;