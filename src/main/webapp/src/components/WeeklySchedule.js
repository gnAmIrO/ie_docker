import React, {useState, useEffect} from "react";
import axios from 'axios'
import EnglishNum2Arabic from '../js/ArabicFilter.js'
import course_types from '../js/CourseTypes.js'
import '../css/styles.css'
import '../css/weekly-schedule.css'


function GenerateHours(minHour, maxHour) {
  var hours = []
  for (var i = minHour; i <= maxHour; i++) {
    hours.push(i + ':00 - ' + (i + 1) + ':00')
  }
  return hours
}

function DaysHeader() {
  const days = ['',
  'یک',
  'دو',
  'سه',
  'چهار',
  'پنج']
  return (
    <div className="row">
      <div className="col-1 cell"></div>
      <div className="col-11">
        <div className="row">
          {days.map(day => <div className="col-sm-2 day"><span>{day + 'شنبه'}</span></div>)}
        </div>
      </div>
    </div>
  )
  
}

const engDays = ['saturday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday']

function RowCourses(props) {
  
  return (
    engDays.map(day => 
      <Course hour={props.hour} day={day} courses={props.courses}/>
    )
  )
}

function Course(props) {
  const courses = props.courses
  const startTime = props.hour.split(' - ')[0]
  var startHour = startTime.replace(/:[3|0]0/g, '')
  if (startHour.length == 1) {
    startHour = '0' + startHour
  }
  // console.log(courses, startTime, startHour, courses[0].classTime.time.begin, courses[0].classTime.time.begin.replace(/:[3|0]0/g, ''))
  const index = courses.findIndex(course => {
    // console.log(courses, startTime, startHour, course.classTime.time.begin, course.classTime.time.begin.replace(/:[3|0]0/g, ''))
    return (course.classTime.days.includes(props.day) && course.classTime.time.begin.replace(/:[3|0]0/g, '') == startHour)
  })
  if (index != -1) {
    const course = courses[index]
    const classTime = EnglishNum2Arabic(course.classTime.time.begin + ' - ' + course.classTime.time.end)
    const classDuration = (Date.parse("1970-01-01 " + course.classTime.time.end) - Date.parse("1970-01-01 " + course.classTime.time.begin)) / 3600000
    
    const isHalf = course.classTime.time.begin.includes(':30') ? 'half ' : ''
    
    var size = '1'
    if (classDuration == 1.5) {
      size = '1-5'
    }
    else if (classDuration == 2) {
      size = '2'
    }
    else if (classDuration == 3) {
      size = '3'
    }
    

    return (
      <div className="col-sm-2 cell">
        <span className={"unit " + isHalf + course.type + " _" + size + " l2r"}><a data-target={'#' + course.id} data-toggle='modal'><span>{classTime}</span><span className="h6 r2l">{course.name}</span> <span className="h6">{course_types[course.type][1]}</span> </a></span>
      </div>
    )
  }
  return (
    <div className="col-sm-2 cell"></div>
  )  
}

function DaysRow(props) {
  const hour = props.hour
  const courses = props.courses

  return (
    <div className="col">
      <div className="row">
        <RowCourses courses={courses} hour={hour}/>        
      </div>
    </div>
  )
  
}

function Hour(props) {
  const time = props.time; 
  const shortHour = EnglishNum2Arabic(time.replace(/:۰۰/g, ''))
  
  return (
    <div className="col-1 time l2r"><span>{time}</span> <span>{shortHour}</span></div>
  );
}

function Rows(props) {
  const hours = GenerateHours(8, 17)
  
  return (
    <div className="box topless">
      <DaysHeader/>
      {hours.map(hour => {
        const persianHour = EnglishNum2Arabic(hour)
        return (
          <div className="row">
            <Hour time={persianHour}/>
            <DaysRow hour={hour} courses={props.courses}/>
          </div>
        )}
      )}
    </div> 
  )
}

function Modal(props) {
  const course = props.course

  return (
    <div class="modal" id={course.id} tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class={"modal-header " + course.type}>
            <span class="modal-title" data-dismiss='modal'>&times;</span>
          </div>
          <div class="modal-body l2r text-center">
            <span>{EnglishNum2Arabic(course.classTime.time.begin + ' - ' + course.classTime.time.end)}</span>
            <span class="h6">{course.name}</span> 
            <span class="h6">{course_types[course.type][1]}</span> 
          </div>
        </div>
      </div>
    </div>
    
  )
}

function WeeklySchedule() {
    const token = localStorage.getItem('token')
    const [semesterNum, setSemesterNum] = useState('۶')
    const [courses, setCourses] = useState([])

    useEffect(() => {
      axios.get('http://87.247.185.122:30379/weekly_schedule', {headers: {
        'Authorization': `Bearer ${token}`
      }})
      .then(res => {
        console.log(res.data)
        setCourses(res.data)
      }
      ).catch(err => {
          console.log(err.response)
          localStorage.removeItem('token');
        })

    }, [token])
    
    return (
      <div>
        <div id="modals">
          {courses.map(course => <Modal course={course}/>)}
        </div>
        <div className="content schedule">
          <div className="table-caption box round-border big-box">
            <div className="pair">
              <div className="pair-key">
                <span><i className="flaticon-calendar"></i></span>
              </div>
              <div className="pair-value">
                <span>برنامه هفتگی</span>
              </div>
            </div>        
            <div>
              <span>ترم ۶</span>
            </div>           
          </div>
          <Rows courses={courses}/> 
        </div>
      </div>
  
    );
}
  
export default WeeklySchedule;