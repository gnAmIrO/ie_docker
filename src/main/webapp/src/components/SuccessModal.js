import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import React from 'react';

function SuccessModal({successMessage}){

  React.useEffect(() => {
    console.log(successMessage)
    if(!successMessage)
      return;
    toast.success(successMessage, {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
  }, [successMessage]);
  
  return (
    <div className={'mt-5'}>
      <ToastContainer
      position="top-center"
      autoClose={3000}
      hideProgressBar={false}
      newestOnTop={false}
      closeOnClick
      rtl
      pauseOnFocusLoss
      draggable
      />
    </div>
    );
}

export default SuccessModal;