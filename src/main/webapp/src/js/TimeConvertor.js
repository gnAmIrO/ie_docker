const convertTime2HH_MM = time => {
    return time.match(/^(\d+)/)[1] + ":" + time.match(/:(\d+)/)[1]
} 


export default convertTime2HH_MM