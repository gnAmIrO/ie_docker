var arabicNumbers = ['۰',
 '۱',
 '۲',
 '۳',
 '۴',
 '۵',
 '۶',
 '۷',
 '۸',
 '۹'];

function EnglishNum2Arabic(s) {
  return s.toString().split('').map(c => Number.isInteger(parseInt(c)) ? arabicNumbers[parseInt(c)] : c).join('')  
}

export  default EnglishNum2Arabic;