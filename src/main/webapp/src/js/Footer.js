import '../css/footer.css';
import '../css/font/flaticon.css'
import React from 'react';

class Footer extends React.Component{
 constructor(props) {
      super(props);
  }
  render (){ 
    return (
    <div className="footer row">
        <div ><p>&copy; دانشگاه تهران - سامانه جامع بلبل‌ستان</p></div>
        <div className="footer-icon">
        <a href="#"><span><i className="flaticon-twitter-logo-on-black-background"></i></span></a>
        <a href="#"><span><i className="flaticon-linkedin-logo"></i></span></a>
        <a href="#"><span><i className="flaticon-instagram"></i></span></a>
        <a href="#"><span><i className="flaticon-facebook"></i></span></a>
        </div>
    </div>
  )};
}

export default Footer;
