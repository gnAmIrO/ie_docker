function GetCurrentStudent() {
    if (localStorage.getItem('token') == null) {
        return '-1'
    }
    return localStorage.getItem('token')
}

export default GetCurrentStudent;