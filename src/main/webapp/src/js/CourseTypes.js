const course_types = {
    'Takhasosi' : ['takhasosi', 'تخصصی'],
    'Asli' : ['asli', 'اصلی'],
    'Paaye' : ['paye', 'پایه'],
    'Umumi' : ['omomi', 'عمومی']
}

export default course_types