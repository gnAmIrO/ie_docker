import '../css/header.css';
import '../css/footer.css';
import '../css/signup-login.css';
import React from 'react';
import { usePromiseTracker } from "react-promise-tracker";
import { trackPromise } from 'react-promise-tracker';

class Login extends React.Component {
  constructor(props) {
      super(props);
      this.handleLoginSubmit = this.handleLoginSubmit.bind(this);
      this.handleSignupSubmit = this.handleSignupSubmit.bind(this);
  }

  handleLoginSubmit(event){
    event.preventDefault();
    console.log('submitted ' + event.target.elements.username.value);
  }

  handleSignupSubmit(event){
    event.preventDefault();
    console.log('submitted ' + event.target.elements.username.value);
  }

  render (){ 
    return (
    <div className="container signup-login-container">
        <div className="row center">
          <div className="col-sm-6 login-form">
            <form onSubmit={this.handleLoginSubmit}>
              <h3>ورود</h3>
              <div className="form-group">
                  <input type="text" className="form-control" placeholder="شماره دانشجویی" name="username"/>
              </div>
              <div className="form-group">
                  <input type="password" className="form-control" placeholder="رمز عبور" name="password" />
              </div>
              <div className="form-group">
                <input type="submit" className="btnSubmit" value="ورود" readOnly/>
              </div>
              <div className="form-group">
                <a href="#" className="btnForgetPwd">پسوردتو فراموش کردی؟</a>
              </div>
            </form>
          </div>
          <div className="col-sm-6 signup-form">
            <form onSubmit={this.handleSignupSubmit}>
              <h3>ثبت نام</h3>
              <div className="signup-login-logo">
                <img src="../images/logo.png" alt="logo pic"/>
              </div>
              <div className="form-group">
                  <input type="text" className="form-control" placeholder="شماره دانشجویی" name="username" />
              </div>
              <div className="form-group">
                  <input type="password" className="form-control" placeholder="رمز عبور" name="password" />
              </div>
              <div className="form-group">
                <input type="password" className="form-control" placeholder="تایید رمز عبور" name="repassword" />
              </div>
              <div className="form-group">
                <input type="submit" className="btnSubmit" value="ثبت نام" readOnly />
              </div>
            </form>
          </div>
        </div>
      </div>
  )};
}

export default Login;
