import React from "react";
import ReactDOM from "react-dom";
// import "./index.css";
import App from "./App";
import reportWebVitals from './js/reportWebVitals';
import './index.css';
import './css/styles.css';
import './css/home.css';
import {Footer} from './components';

ReactDOM.render(<App />, document.getElementById("root"));
ReactDOM.render(<Footer />, document.getElementById("footer"));


{/** */}

reportWebVitals();
