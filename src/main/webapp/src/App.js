import React from "react";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import { Login, ResetPassword, Header, Home, Courses, WeeklySchedule } from "./components";

const isAuthenticate = () => {
  const authStr = localStorage.getItem('token');
  if(!authStr)
    return false;
  
  return true;
}

function App() {
  return (
      <Router>
        <Switch>
          <Route path="/" exact > {isAuthenticate() ? <><Header links={['/courses', '/weekly_schedule']} addresses={['انتخاب واحد', 'برنامه هفتگی']}/> <Home /></> : <Redirect to={'/login'} />} </Route>
          <Route path="/courses" exact > {isAuthenticate() ?<><Header links={['/', '/weekly_schedule']} addresses={['خانه', 'برنامه هفتگی']}/> <Courses /> </>: <Redirect to={'/login'} />}</Route>
          <Route path="/weekly_schedule" exact > {isAuthenticate() ?<><Header links={['/', '/courses']} addresses={['خانه', 'انتخاب واحد']}/> <WeeklySchedule /> </>: <Redirect to={'/login'} />}</Route>
          <Route path="/login" exact > {!isAuthenticate() ? <Login /> : <Redirect to={'/'} />} </Route>
          <Route path="/resetPassword" exact component={() => <ResetPassword />} />
        </Switch>
      </Router>
  );
}

export default App;